require 'tasks/databinder.rb'

MYSQL='mysql:mysql-connector-java:jar:5.1.6'
HQLDB='hsqldb:hsqldb:jar:1.8.0.7'

repositories.remote << "http://repo1.maven.org/maven2/"

# to run with embedded server logging to console, do
# buildr basic-hib:run

desc "Databinder basic application template"
define "basic-hib" do

  project.version = "1.0-SNAPSHOT"
  project.group = "template"
  
  clean { rm_rf _("dep_preview") }
  build dep_preview('net/databinder/components/DataStyleSheet.css', DATABINDER_COMPONENTS)

  compile.options.target = '1.5'

  resources.from('src/main/java').exclude('**/*.java')
  
  # switch below for MYSQL or other if desired,
  # also see src/main/java/hibernate.properties
  compile.with [DATABINDER_HIB, HQLDB]
  
  # adds run, start, and stop tasks from databinder.rake
  # default port is 8080; use JETTY_PORT env variable to override
  embed_server

  package :jar

end
