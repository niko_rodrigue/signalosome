

all : 
	mvn -Djetty.port=9999 compile exec:java

clean :
	mvn clean:clean	 

war :
	mvn package

run :
	mvn jetty:run -Djetty.port=9999