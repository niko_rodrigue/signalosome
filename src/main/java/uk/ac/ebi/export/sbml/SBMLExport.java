package uk.ac.ebi.export.sbml;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.databinder.models.hib.HibernateListModel;

import org.apache.wicket.model.util.ListModel;
import org.sbml.libsbml.CVTerm;
import org.sbml.libsbml.Compartment;
import org.sbml.libsbml.ListOfSpecies;
import org.sbml.libsbml.Model;
import org.sbml.libsbml.ModelCreator;
import org.sbml.libsbml.ModelHistory;
import org.sbml.libsbml.ModifierSpeciesReference;
import org.sbml.libsbml.Reaction;
import org.sbml.libsbml.SBMLDocument;
import org.sbml.libsbml.SBMLWriter;
import org.sbml.libsbml.SBase;
import org.sbml.libsbml.Species;
import org.sbml.libsbml.SpeciesReference;
import org.sbml.libsbml.libsbmlConstants;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Comment;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.Participant;

/**
 * Creates an SBML file from an Interaction
 * 
 * @author skeating
 *
 */

public class SBMLExport implements Serializable {

	private static final String SBO_URI = "urn:miriam:obo.sbo";
	private static final String SIGNALOSOME_CONTAINER_ONTOLOGY_URI = Annotated.SIGNALOSOME_CONTAINER_ONTOLOGY_URN;
	
	static int metaidcount = 1;
	static String defaultCompartment = "default_spine";
	static ArrayList<String> speciesAdded;

	/**
   * Default constructor
   */
  public SBMLExport()
  {
    // reset the metaid counter
    metaidcount = 1;
  }

  public void writeSBML(HibernateListModel<Component> componentListModel,
                        HibernateListModel<Parameter> parameterListModel,
                        HibernateListModel<Interaction> interactionListModel, File outFile)
  {
    if (speciesAdded == null)
    {
      speciesAdded = new ArrayList<String>();
    }
    else
    {
      speciesAdded.clear();
    }
    List<uk.ac.ebi.compneur.signalosome.data.Component> allComponents = componentListModel.getObject();
    List<uk.ac.ebi.compneur.signalosome.data.Parameter> allParameters = parameterListModel.getObject();
    List<uk.ac.ebi.compneur.signalosome.data.Interaction> allInteractions = interactionListModel.getObject();

    // create a document containing a model
    SBMLDocument d = new SBMLDocument(3, 1);
    Model m = d.createModel();

    // add a model history
    addModelHistory(m);

    //for (int i = 0; i < allComponents.size(); i++)
    //{
    //   m.addSpecies(createSBMLSpeciesFromComponent(allComponents.get(i), defaultCompartment));
    //}
   
    for (int i = 0; i < allParameters.size(); i++)
    {
       m.addParameter(createSBMLParameterFromParameter(allParameters.get(i)));
    }

    for (int i = 0; i < allInteractions.size(); i++)
    {
	    Reaction r = createSBMLReactionFromInteraction(allInteractions.get(i), defaultCompartment);
      // if there are no participants do not add as this is invalid sbml
	    if (!(r.getNumReactants() == 0 && r.getNumProducts() == 0))
      {
        m.addReaction(r);
        ListOfSpecies los = getSBMLListOfSpeciesFromInteraction(allInteractions.get(i), defaultCompartment);
        for (int j = 0; j < los.size(); j++)
        {
          // it is possible that a species may be a reactant/product in some reactions and a modifier in others
          // when added as a modifier the constant flag is set to true
          // need to catch this since if it is product/reactant it should be false
          if (speciesAdded.contains(los.get(j).getId()))
          {
            if (los.get(j).getConstant() == false)
            {
              Species s = m.getSpecies(los.get(j).getId());
              if (s.getConstant() == true)
              {
                s.setConstant(false);
              }
            }
          }
          else
          {
            m.addSpecies(los.get(j));
            speciesAdded.add(los.get(j).getId());
          }
        }
      }
    }

	  for (int i = 0; i < allComponents.size(); i++)
	  {
	    // check whether the species has been included via an interaction
	    if (speciesAdded.contains(allComponents.get(i).getId()) != true)
	    {
	        Species s = createSBMLSpeciesFromComponent(allComponents.get(i), defaultCompartment);
	        if (s != null) { 
	            m.addSpecies(s);
	        }
	    }
	  }
    createReferencedCompartments(m);

    writeFile(d, outFile); 
  }

  public void writeAsSBMLModel(Annotated annotated, File outFile)
  {
    if (speciesAdded == null)
    {
      speciesAdded = new ArrayList<String>();
    }
    else
    {
      speciesAdded.clear();
    }

    if (annotated instanceof Component)
    {
      writeComponentAsSBMLModel((Component)annotated, outFile);
    }
    else if (annotated instanceof Interaction)
    {
      writeInteractionAsSBMLModel((Interaction)annotated, outFile);
    }
    else if (annotated instanceof Parameter)
    {
      writeParameterAsSBMLModel((Parameter)annotated, outFile);
    }
  }
  private void writeFile(SBMLDocument d, File outFile)
  {
    if (d != null && outFile != null)
    {
        SBMLWriter writer = new SBMLWriter();
        writer.writeSBML(d, outFile.getAbsolutePath());
    }
  }

  private void writeInteractionAsSBMLModel(Interaction interaction, File outFile)
  {   
    // create a document containing a model
    SBMLDocument d = new SBMLDocument(3, 1);
    Model m = d.createModel();

    // add a model history
    addModelHistory(m);

    //// create a reaction with the id of this interaction
    m.addReaction(createSBMLReactionFromInteraction(interaction, defaultCompartment));
    ListOfSpecies los = getSBMLListOfSpeciesFromInteraction(interaction, defaultCompartment);
    for (int i = 0; i < los.size(); i++)
    {
      m.addSpecies(los.get(i));
    }

    createReferencedCompartments(m);

    writeFile(d, outFile);
  }

  private void writeComponentAsSBMLModel(Component component, File outFile)
  {   
	  if (component == null)
	  {
		  return;
	  }
    // create a document containing a model
    SBMLDocument d = new SBMLDocument(3, 1);
    Model m = d.createModel();

    // add a model history
    addModelHistory(m);
    
    m.addCompartment(createSBMLCompartment(defaultCompartment));
			
    m.addSpecies(createSBMLSpeciesFromComponent(component, defaultCompartment));

    writeFile(d, outFile);
  }

  private void writeParameterAsSBMLModel(Parameter parameter, File outFile)
  {
    // create a document containing a model
    SBMLDocument d = new SBMLDocument(3, 1);
    Model m = d.createModel();

    // add a model history
    addModelHistory(m);

    m.addParameter(createSBMLParameterFromParameter(parameter));

    writeFile(d, outFile);
  }

  private org.sbml.libsbml.Parameter createSBMLParameterFromParameter(Parameter parameter)
  {
    // create a parameter with the id of this component
    org.sbml.libsbml.Parameter p = new org.sbml.libsbml.Parameter(3, 1);
    p.setId(parameter.getId());
    p.setName(parameter.getName());
    p.setConstant(true);

    //TO DO value and units
    p.setValue(parameter.getValueMin());

    // add annotations
    addAnnotationsToSBMLObject(parameter.getAnnotations(), (SBase)p);
    addNotesToSBMLObject(parameter.getComments(), (SBase)p);

    setNotesForParameterValues(parameter, (SBase)p);

    return p;
  }

  private void createReferencedCompartments(Model m)
  {
    for (int i = 0; i < m.getNumSpecies(); i++)
    {
      String compartment = m.getSpecies(i).getCompartment();
      if (m.getCompartment(compartment) == null)
      {
        m.addCompartment(createSBMLCompartment(compartment));
      }
    }
  }

  private Compartment createSBMLCompartment(String comp_name)
  {
    Compartment c = new Compartment(3, 1);
    c.setSpatialDimensions(3);
    c.setConstant(true);
    c.setId(comp_name);
    c.setSize(1.0);
     // to do units
    return c;
  }

  private Species createSBMLSpeciesFromComponent(Component component, String comp_name)
  {
	  if (component == null) 
	  {
		  return null;
	  }
    // create a species with the id of this component
    String id = component.getId();
    speciesAdded.add(id);

    Species s = new Species(3, 1);
    s.setHasOnlySubstanceUnits(false);
    s.setBoundaryCondition(false);
    s.setConstant(false);
    s.setId(id + "_" + comp_name);
    s.setName(component.getName());
    s.setCompartment(comp_name);

    // if called from inside an interaction
    // does not yet work as get a session closed exception when trying
    // to query the actual annotation - and causes an exception
    // very weird since it works if exactly one comment
    // add annotations
    
    addAnnotationsToSBMLObject(component.getAnnotations(), (SBase)s);
    addNotesToSBMLObject(component.getComments(), (SBase)s);

    return s;
  }

  private Reaction createSBMLReactionFromInteraction(Interaction interaction, String comp_name)
  {
    // create a reaction
    Reaction r = new Reaction(3, 1);
    r.setId(interaction.getId());
    r.setName(interaction.getName());
    r.setReversible(false); 
    r.setFast(false);
    
    // add annotations
    addAnnotationsToSBMLObject(interaction.getAnnotations(), (SBase) r);
    addNotesToSBMLObject(interaction.getComments(), (SBase) r);

    // from the interaction get each of the particpants
    // add it as a species and either a product/reactant/modifier
    List<Participant> participants = interaction.getParticipants();
    for (Participant p : participants)
    {
	    Annotation locationAnno = p.getContainerAnnotation();
      String location = "";
      
      if (locationAnno != null) {
      	location = locationAnno.getMiriamId();
      }
      if (location.length() == 0)
      {
        location = comp_name;
      }
      else
      {
        location = location.replaceAll(" ", "_");
      }
      if (p.getComponent() == null) 
      {
    	  break;
      }
      
      String p_id = p.getComponent().getId() + "_" + location;

      double stoichiometry = p.getStoichiometry();
      if (stoichiometry < 0)
      {
        SpeciesReference sr = r.createReactant();
        sr.setSpecies(p_id);
        sr.setStoichiometry(-stoichiometry);
        sr.setConstant(true);
      }
      else if (stoichiometry == 0)
      {
        ModifierSpeciesReference sr = r.createModifier();
        sr.setSpecies(p_id);
      }
      else
      {
        SpeciesReference sr = r.createProduct();
        sr.setSpecies(p_id);
        sr.setStoichiometry(stoichiometry);
        sr.setConstant(true);
      }
    }
    return r;
  }

  private ListOfSpecies getSBMLListOfSpeciesFromInteraction(Interaction interaction, String comp_name)
  {
    ListOfSpecies los = new ListOfSpecies(3, 1);
    // from the interaction get each of the particpants
    // add it as a species 
    List<Participant> participants = interaction.getParticipants();
    for (Participant p : participants)
    {
	    Annotation locationAnno = p.getContainerAnnotation();
      String location = "";
      
      if (locationAnno != null) {
      	location = locationAnno.getMiriamId();
      }
      if (location.length() == 0)
      {
        location = comp_name;
      }
      else
      {
        location = location.replaceAll(" ", "_");
      }

      Species s = createSBMLSpeciesFromComponent(p.getComponent(), location);

      if (s == null) 
      {
    	  break;
      }
      
      double stoichiometry = p.getStoichiometry();
      if (stoichiometry == 0)
      {
        s.setConstant(true);
      }

      los.append(s);
    }

    return los;
  }
  private CVTerm createCVTerm(Annotation annotation)
  {
    CVTerm cvterm = new CVTerm();
    cvterm.setQualifierType(libsbmlConstants.BIOLOGICAL_QUALIFIER);

    int i = -1;
    String name = annotation.getQualifier();
    if (name.compareTo("is") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS);
    else if (name.compareTo("hasPart") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_HAS_PART);
    else if (name.compareTo("isPartOf") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_PART_OF);
    else if (name.compareTo("isVersionOf") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_VERSION_OF);
    else if (name.compareTo("hasVersion") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_HAS_VERSION);
    else if (name.compareTo("isHomologTo") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_HOMOLOG_TO);
    else if (name.compareTo("isDescribedBy") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_DESCRIBED_BY);
    else if (name.compareTo("isEncodedBy") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_ENCODED_BY);
    else if (name.compareTo("encodes") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_ENCODES);
    else if (name.compareTo("occursIn") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_OCCURS_IN);
    else if (name.compareTo("isPropertyOf") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_IS_PROPERTY_OF);
    else if (name.compareTo("hasProperty") == 0)
      i = cvterm.setBiologicalQualifierType(libsbmlConstants.BQB_HAS_PROPERTY);

    i = cvterm.addResource(annotation.getUrn());
    
    // TODO : always one URI per qualifier at the moment ?? What about set of URI
    
    if (i == libsbmlConstants.LIBSBML_OPERATION_FAILED) {
    	System.out.println("WARNING : createCVTerm : a CVTerm method as failed !! ");
    }
    
    return cvterm;
  }

  private String getUniqueMetaId()
  {
    String num = Integer.toString(metaidcount);
    while (num.length() < 6)
    {
      num = "0".concat(num);
    }
    num = "_".concat(num);
    metaidcount++;
    return num;
  }

  private void addAnnotationsToSBMLObject(List<Annotation> annotations, SBase object)
  {
    if (annotations != null)
    {
      object.setMetaId(getUniqueMetaId());
      
      for (Annotation ann: annotations)
      {
    	  if (ann.getMiriamURI().equals(SBO_URI)) {
    		  // use annotation to set the SBO term of the SBase object
    		  System.out.println("setting the SBO term '" + ann.getMiriamId() + "' on the SBase '" + object + "'");
    		  object.setSBOTerm(ann.getMiriamId());
    	  } else if (ann.getMiriamURI().equals(SIGNALOSOME_CONTAINER_ONTOLOGY_URI)) {
    		  // ignore annotation, it is used elsewhere.
    	  }
    	  else 
    	  {
    		  // add the annotation to the SBase object
    		  CVTerm cv = createCVTerm(ann);
    		  if (cv != null){
    			  object.addCVTerm(cv);
    		  }
    	  }
      }
    }
  }

  private void addModelHistory(Model m)
  {
    m.setMetaId(getUniqueMetaId());
    ModelHistory h = new ModelHistory();

    ModelCreator c = new ModelCreator();
    c.setFamilyName("Computational Neurobiology Group");
    c.setGivenName(" ");
    c.setEmail("compneur@ebi.ac.uk");
    c.setOrganisation("EMBL-EBI");

    h.addCreator(c);

    org.sbml.libsbml.Date date = new org.sbml.libsbml.Date(getDateTime());

    h.setCreatedDate(date);
    h.setModifiedDate(date);

    m.setModelHistory(h);
  }

  private String getDateTime()
  {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat timeFormat = new SimpleDateFormat("HH-mm-ss");
    DateFormat zoneFormat = new SimpleDateFormat("Z");
    Date date = new Date();
    String zone = zoneFormat.format(date);
    String sbml_zone = zone.substring(0, 3) + ":" + zone.substring(3); 
    return (dateFormat.format(date) + "T" + timeFormat.format(date) + sbml_zone);
  }

  private void setNotesForParameterValues(Parameter parameter, SBase object)
  {
    String body_start = "<body xmlns=\"http://www.w3.org/1999/xhtml\">";
    String notes = null;
    notes = body_start;
    notes = notes.concat("\n<p> Values listed for this parameter are: </p>\n");
    notes = notes.concat("\n<p> Minimum value: " + parameter.getValueMin() + "; Maximum value: " + parameter.getValueMax() + " </p>\n");
    notes = notes.concat("\n<p> Mean value: " + parameter.getValueMean() + "; Standard deviation: " + parameter.getStd() + " </p>\n");
    notes = notes.concat("\n<p> Units: " + parameter.getUnit() + " </p>\n");
    notes = notes.concat("\n</body>\n");

    object.appendNotes(notes);
  }

  private void addNotesToSBMLObject(List<Comment> comments, SBase object)
  {
    String body_start = "<body xmlns=\"http://www.w3.org/1999/xhtml\">";
    String notes = null;
    if (comments != null && comments.size() > 0)
    {
      notes = body_start;
      for (Comment comm: comments)
      {
        notes = notes.concat(("\n<p>On " + comm.getDateString() + " " + comm.getUser() + " commented: " + comm.getComment() + "</p>")); 
      }
      notes = notes.concat("\n</body>\n");

      object.setNotes(notes);
    }

}

  static void println(String msg)
  {
    System.out.println(msg);
  }
    ///**
    // * Loads the SWIG-generated libSBML Java module when this class is
    // * loaded, or reports a sensible diagnostic message about why it failed.
    // */
  static
  {
    String varname;

    if (System.getProperty("mrj.version") != null)
      varname = "DYLD_LIBRARY_PATH";	// We're on a Mac.
    else
      varname = "LD_LIBRARY_PATH";	// We're not on a Mac.

    try
    {
      System.loadLibrary("sbmlj");
      // For extra safety, check that the jar file is in the classpath.
      Class.forName("org.sbml.libsbml.libsbml");
    }
    catch (UnsatisfiedLinkError e)
    {
      System.err.println("Error: could not link with the libSBML library." +
       "  It is likely\nyour " + varname +
       " environment variable does not include\nthe" +
       " directory containing the libsbml library file.");
      System.exit(1);
    }
    catch (ClassNotFoundException e)
    {
      System.err.println("Error: unable to load the file libsbmlj.jar." +
       "  It is likely\nyour " + varname + " environment" +
       " variable or CLASSPATH variable\ndoes not include" +
       " the directory containing the libsbmlj.jar file.");
      System.exit(1);
    }
    catch (SecurityException e)
    {
      System.err.println("Could not load the libSBML library files due to a" +
       " security exception.");
    }
  }



}
