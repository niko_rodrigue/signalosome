package uk.ac.ebi.upload.sbml;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.swing.tree.TreeNode;
import javax.xml.stream.XMLStreamException;

import net.databinder.models.hib.HibernateListModel;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.SBase;
import org.sbml.jsbml.SimpleSpeciesReference;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.SpeciesReference;
import org.sbml.jsbml.util.filters.Filter;
import org.sbml.jsbml.xml.XMLNode;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Comment;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.Participant;
import uk.ac.ebi.compneur.signalosome.util.HibernateUtil;

/**
 * Imports an SBML file into the database, assuming the DB is empty
 * 
 * @author rodrigue
 *
 */

// TODO : update species and reaction IDs in cellDesigner annotation when changing them !!!

public class SBMLImport implements Serializable {

	public static final String USER_NAME = "martinaf";
	private static final String SBO_URI = "urn:miriam:obo.sbo";
	private static final String SIGNALOSOME_CONTAINER_ONTOLOGY_URI = Annotated.SIGNALOSOME_CONTAINER_ONTOLOGY_URN;

	static String defaultCompartment = "default_spine";

	/**
	 * Default constructor
	 */
	public SBMLImport()
	{
	}

	public void writeSBML(HibernateListModel<Component> componentListModel,
			HibernateListModel<Parameter> parameterListModel,
			HibernateListModel<Interaction> interactionListModel, File outFile)
	{
	}




	static void println(String msg)
	{
		System.out.println(msg);
	}



	/**
	 * @param args
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public static void main(String[] args) throws XMLStreamException, IOException {

		Logger logger = Logger.getLogger(SBMLImport.class);

		if (args.length == 0)
		{
			// TODO : print usage info
			System.out.println("You need to give one SBML file as input");
			return;
		}
		SBMLReader reader = new SBMLReader();
		SBMLDocument doc = reader.readSBMLFromFile(args[0]);
		Model model = doc.getModel();
		Session session = HibernateUtil.getHibernateSession();

		// Map between the SBML species name as key and the SBML species as value, to avoid putting twice the same name in the DB.
		// Only one Signalosome Component for several potential SBML Species.
		HashMap<String, Species> speciesNameMap = new HashMap<String, Species>();

		// Map between the SBML species id as key and the Signlosome Component as value.
		// Will allow to retrieve the correct Component easily when looking at SBML SpeciesReference and to update the cellDesigner annotations
		HashMap<String, Component> speciesIdMap = new HashMap<String, Component>();

		// Adding Signalosome Component from SBML Species
		for (Species species : model.getListOfSpecies()) 
		{
			String name = species.getName();

			System.out.println("Species : name = '" + name + "',  id = '" + species.getId() + "'," 
					+ " compartment = '" + species.getCompartment());

			if (! speciesNameMap.containsKey(name)) 
			{
				speciesNameMap.put(name, species);

				session = HibernateUtil.getHibernateSession();
				session.beginTransaction();
				Component component = new Component();
				component.setName(name);
				component.setUserId(USER_NAME);
				session.save(component);

				session.getTransaction().commit();

				speciesIdMap.put(species.getId(), component);
		  			
	  			importAnnotations(species, component);
	  			
	  			importComments(species, component);
	  			
	  			// TODO : store any custom annotations
	  			if (species.isSetAnnotation() && species.getAnnotation().isSetNonRDFannotation())
	  			{
//	  				System.out.println("Additional annotations :\n@" + species.getAnnotation().getNonRDFannotation() + "@");
	  			}
	  		}
	  		else 
	  		{
	  			System.out.println("WARNING : name '" + name + "' already present. Won't be added twice");

	  			session = HibernateUtil.getHibernateSession();
	  			session.beginTransaction();
	  			
	  			Component component = HibernateUtil.findComponentByName(species.getName());
	  			System.out.println("Component with id '" + component.getId() + "' is used.");
	  			speciesIdMap.put(species.getId(), component);
		  			
	  			session.getTransaction().commit();
	  			
	  			// TODO : check that the annotations are the same !!
	  			
	  			// TODO : check is more notes or non rdf annotations to store
	  			
	  		}
	  		

	  	}
	  	
		// TODO : parameters ???
	  	
	  	// Adding Signalosome Interaction from SBML Reaction
	  	for (Reaction reaction : model.getListOfReactions()) 
	  	{
	  		session = HibernateUtil.getHibernateSession();
	        session.beginTransaction();
	        
	        String reactionName = reaction.isSetName() ? reaction.getName() : reaction.getId();
	        
	        System.out.println("Adding reaction '" + reactionName + "'");
	        
	        Interaction interaction = new Interaction();
	        interaction.setName(reactionName);
	        interaction.setUserId(USER_NAME);
			session.save(interaction);
			session.getTransaction().commit();
			
			importAnnotations(reaction, interaction);
			importComments(reaction, interaction);
			
  			// TODO : store any custom annotations
  			if (reaction.isSetAnnotation() && reaction.getAnnotation().isSetNonRDFannotation())
  			{
  				// System.out.println("Additional annotations :\n@" + reaction.getAnnotation().getNonRDFannotation() + "@");
  			}

  			
  			for (SpeciesReference speciesReference : reaction.getListOfReactants())
  			{
  	  			importSpeciesReference(speciesIdMap, interaction, speciesReference, true);				
  			}
  			for (SpeciesReference speciesReference : reaction.getListOfProducts())
  			{
  	  			importSpeciesReference(speciesIdMap, interaction, speciesReference, false);				
  			}
  			for (SimpleSpeciesReference speciesReference : reaction.getListOfModifiers())
  			{
  	  			importSpeciesReference(speciesIdMap, interaction, speciesReference, null);				
  			}


	  	}

	}

	private static void importSpeciesReference(
			HashMap<String, Component> speciesIdMap, Interaction interaction,
			SimpleSpeciesReference speciesReference, Boolean isLeft) 
	{
		Session session;
		session = HibernateUtil.getHibernateSession();
		session.beginTransaction();

		Participant participant = new Participant();
		
		if (speciesReference instanceof SpeciesReference)
		{
			participant.setLeft(isLeft);
			participant.setStoichiometry(((SpeciesReference) speciesReference).getStoichiometry());
		}
		
		participant.setInteraction(interaction);
		participant.setUserId(USER_NAME);
		participant.setComponent(speciesIdMap.get(speciesReference.getSpecies()));

		session.save(participant);
		interaction.addParticipant(participant);

		String containerName = speciesReference.getSpeciesInstance().getCompartmentInstance().getName(); 

		// add the location/container annotation.
		if (containerName != null && containerName.trim().length() > 0) {
			Annotation locationAnno = new Annotation();
			locationAnno.setUrn(uk.ac.ebi.compneur.signalosome.data.Annotated.SIGNALOSOME_CONTAINER_ONTOLOGY_URN + ":" + containerName);
			locationAnno.setExternalId(participant.getId());
			locationAnno.setExternalType("participant");
			locationAnno.setUserId(USER_NAME);
			locationAnno.setQualifier("occursIn");

			session.saveOrUpdate(locationAnno);		
			participant.addAnnotation(locationAnno);	

			// TODO : check that the name is a valid one !!! 
		}
		
		// TODO : add any custom non-RDF annotations
		
		session.getTransaction().commit();
	}

	private static void importAnnotations(SBase sbase, Annotated annotated) 
	{
		Session session;

		// Adding annotations
		if (sbase.getCVTermCount() > 0)
		{
			int i = 1;
			for (CVTerm cvterm : sbase.getAnnotation().getListOfCVTerms())
			{
				String qualifier = cvterm.isBiologicalQualifier() ? cvterm.getBiologicalQualifierType().getElementNameEquivalent()
						: cvterm.getModelQualifierType().getElementNameEquivalent();

				for (String uri : cvterm.getResources())
				{					

					session = HibernateUtil.getHibernateSession();
					session.beginTransaction();

					Annotation annotation = new Annotation();

					annotation.setExternalId(annotated.getId());
					annotation.setExternalType("component");
					annotation.setQualifier(qualifier);
					annotation.setUserId(USER_NAME);
					annotation.setUrn(uri);
					annotation.setIndex(i);

					session.save(annotation);

					annotated.addAnnotation(annotation);
					session.getTransaction().commit();
				}
				i++;
			}
		}

		// TODO : check for sboTerm and add it as annotation if present
	}

	private static void importComments(SBase sbase, Annotated annotated) 
	{
		Session session;

		if (sbase.isSetNotes())
		{
			session = HibernateUtil.getHibernateSession();
			session.beginTransaction();

			XMLNode notesXmlNode = sbase.getNotes();

			// Searching for the 'body' html element in the notes
			List<? extends TreeNode> bodyNodes = notesXmlNode.filter(new Filter() 
			{
				@Override
				public boolean accepts(Object o) 
				{
					if (o instanceof XMLNode)
					{
						XMLNode xmlNode = (XMLNode) o;

						if (xmlNode.isElement() && xmlNode.getName().equals("body"))
						{
							return true;
						}
					}

					return false;
				}
			});

			if (bodyNodes.size() == 1)
			{
				// System.out.println("found 'body' element :-) !!");
				XMLNode bodyNode = (XMLNode) bodyNodes.get(0);

				for (int i = 0; i < bodyNode.getChildCount(); i++)
				{
					XMLNode xmlNode = bodyNode.getChildAt(i);

					if (xmlNode.isText() && xmlNode.getCharacters().trim().length() == 0)
					{
						continue;
					}

					String comment = xmlNode.toXMLString();
					// System.out.println("Comment " + i + " = '" + comment + "'");

					Comment commentObj = new Comment();
					commentObj.setComment(comment.replace("'", "\'"));
					commentObj.setUser(USER_NAME);

					commentObj.setDate(Calendar.getInstance().getTime());

					commentObj.setExternalId(annotated.getId());
					commentObj.setExternalType("component");

					session.save(commentObj);
					annotated.addComment(commentObj);
				}
			}
			else
			{
				System.out.println("WARNING : found some notes without body element !!!!");
				System.out.println(notesXmlNode.toXMLString());
			}

			session.getTransaction().commit();
		}
	}

}
