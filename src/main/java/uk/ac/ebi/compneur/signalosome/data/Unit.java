package uk.ac.ebi.compneur.signalosome.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unit")
public class Unit implements Serializable {

	@Id
	private String id;
	private String name;
	private String symbol;
	private String quantity;
	
	@Column(name="exp_SI_base_unit")
	private String expressionBaseSIUnit;

	@Column(name="tag")
	private String tags;

	public Unit() {
		
	}
	
	public Unit(String id, String name, String symbol, String quantity,
			String expressionBaseSIUnit, String tags) {
		super();
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.quantity = quantity;
		this.expressionBaseSIUnit = expressionBaseSIUnit;
		this.tags = tags;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getQuantity() {
		return quantity;
	}

	public String getExpressionBaseSIUnit() {
		return expressionBaseSIUnit;
	}

	public String getTags() {
		return tags;
	}
	
	
}
