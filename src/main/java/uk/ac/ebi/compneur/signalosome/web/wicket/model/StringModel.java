package uk.ac.ebi.compneur.signalosome.web.wicket.model;

import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.apache.wicket.model.IModel;

public class StringModel  implements IModel<String> {

	@Transient private transient Logger logger = Logger.getLogger(this.getClass());
	
	private String modelObject;		


	public StringModel() {
	}

	public StringModel(String object) {
		modelObject = object;
	}

	public void detach() {
		modelObject = null;
	}


	public String getObject() {
		
		logger.debug("getObject : object = " + modelObject);
		
		return modelObject;
	}


	public void setObject(String object) {
		
		logger.debug("setObject : object = " + object);
		
		modelObject = object;
	}

	
	
}
