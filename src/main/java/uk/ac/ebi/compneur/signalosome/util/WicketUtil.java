package uk.ac.ebi.compneur.signalosome.util;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.util.iterator.ComponentHierarchyIterator;

public class WicketUtil {

	
    public static void listComponentChildren(MarkupContainer wcomponent) 
    {
      ComponentHierarchyIterator childrenIterator = wcomponent.visitChildren();
      
      System.out.println("\n Does the MarkupContainer has children : " + childrenIterator.hasNext() + "\n");
      
      while (childrenIterator.hasNext()) 
      {
      	org.apache.wicket.Component childComponent = childrenIterator.next();
      	System.out.println("container child : '" + childComponent);
      }
      

    }

}
