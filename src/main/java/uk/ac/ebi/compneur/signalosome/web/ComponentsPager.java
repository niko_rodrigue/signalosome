package uk.ac.ebi.compneur.signalosome.web;

import java.util.ArrayList;
import java.util.List;

import net.databinder.models.hib.CriteriaFilterAndSort;
import net.databinder.models.hib.HibernateListModel;
import net.databinder.models.hib.HibernateProvider;

import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilteredAbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.GoAndClearFilter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.AnnotatedLinkPanel;


/**
 * Displays paginated, filtered, and sorted Component objects.
 */
@SuppressWarnings("rawtypes")
public class ComponentsPager extends BasePage
{
	// Cached list of Component ??
	HibernateListModel<Component> components = new HibernateListModel<Component>("from Component order by name");
	
	@SuppressWarnings("unchecked")
	public ComponentsPager()
	{
		super();

		// ((DataApplication) getApplication()).getHibernateSessionFactory(null).getCurrentSession().beginTransaction();
		
		// This Query retrieve the list of Interactions
		// HibernateListModel<Interaction> interactions = new HibernateListModel<Interaction>("from interaction order by name");

		// list of columns allows table to render itself without any markup from us
		List<IColumn<Component>> columns = new ArrayList<IColumn<Component>>();
		
		columns.add(new FilteredAbstractColumn(new Model("Actions")) { // use "" for no column header
                    public void populateItem(Item cellItem, String componentId, IModel rowModel)
                    {
                        cellItem.add(new AnnotatedLinkPanel(componentId, rowModel));
                    }
					// return the go-and-clear filter for the filter toolbar
					public org.apache.wicket.Component getFilter(String componentId, FilterForm form)
					{
						return new GoAndClearFilter(componentId, form);
					}
                });
		
		columns.add(new TextFilteredPropertyColumn(new Model<String>("Identifier"), "id", "id"));
		
		columns.add(new TextFilteredPropertyColumn(new Model<String>("Name"), "name", "name"));
		columns.add(new TextFilteredPropertyColumn(new Model<String>("UserId"), "userId", "userId"));
		
		// custom column to enable components to be in the list and labeled correctly
		//new ObjectFilteredPropertyColumn(new Model("Birth country"), "country.name", "country.name", "country", "name",components),
		//new ObjectFilteredPropertyColumn(new Model("Birth city"), "birthCity.name", "birthCity.name", "birthCity", "name",interactions),
		//new TextFilteredPropertyColumn(new Model("Weight (lbs)"), "weight", "weight"),
		//new TextFilteredPropertyColumn(new Model("Height (inches)"), "height", "height"),
		//new PropertyColumn(new Model("Final game"), "finalGame", "finalGame")
		
		
		CriteriaFilterAndSort builder = new CriteriaFilterAndSort(new uk.ac.ebi.compneur.signalosome.data.Component(), "name", true, false);
		FilterForm form = new FilterForm("form", builder);
		
		HibernateProvider provider = new HibernateProvider(uk.ac.ebi.compneur.signalosome.data.Component.class, builder);
		provider.setWrapWithPropertyModel(false);
		
		DataTable table = new DataTable("components", columns, provider, 50) {
			@Override protected Item newRowItem(String id, int index, IModel model)
			{
				return new OddEvenItem(id, index, model);
			}
		};
		table.addTopToolbar(new AjaxNavigationToolbar(table));
		table.addTopToolbar(new FilterToolbar(table, form, builder));
		table.addTopToolbar(new AjaxFallbackHeadersToolbar(table, builder));
		table.addBottomToolbar(new AjaxNavigationToolbar(table));
		add(form.add(table));
		
		// ((DataApplication) getApplication()).getHibernateSessionFactory(null).getCurrentSession().disconnect();
	}
}
