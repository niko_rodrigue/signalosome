package uk.ac.ebi.compneur.signalosome.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


/**
 * <p>Parameter
 * 
 * @author Nicolas Rodriguez and Camille Laibe
 */
@Entity
@Table(name="parameter")
@Audited
public class Parameter extends Annotated implements Serializable
{
	// TODO : see if we can use the ejbLoad() and ejbStore() methods to load by hand the Annotated Object
	// if the @Any is failing
	// TODO : see if defining a hibernate UserType could help ?
	
    @Transient
    private static final long serialVersionUID = -3210671216232611582L;
    
    @Column(name = "value_min")
    private double valueMin;
    @Column(name = "value_max")
    private double valueMax;
    @Column(name = "value_mean")
    private double valueMean;
    @Column(name = "std")
    private double std;
    
    // Should we try to store the unit as a Unit object ??
    private String unit;
    

    /*
     // Not working, doing this outside of the parameter class will be easier.
      
    // @AuditOverride(name="annotated", isAudited=false)
    @Any(metaColumn = @Column( name = "external_type" ), fetch=FetchType.EAGER )
    @AnyMetaDef( 
            idType = "string", 
            metaType = "string", 
            metaValues = {
                @MetaValue( value = "component", targetEntity = Component.class ),
                @MetaValue( value = "interaction", targetEntity = Interaction.class )
            } )
        @JoinColumn( name = "external_id" )
    @JoinTable(name="param_rel",
        joinColumns = @JoinColumn(name="parameter_id"),
        inverseJoinColumns = @JoinColumn(name="external_id")
    )
    private Annotated annotated;
    */
    
    /**
     * Default Constructor.
     */
    public Parameter()
    {
        super();
    }
    
    /**
     * Constructor
     * @param name
     */
    public Parameter(String name)
    {
    	this.name = name;
    }
    
    /**
     * Constructor
     * @param id
     * @param name
     */
    public Parameter(String id, String name)
    {
    	this.id = id;
    	this.name = name;
    }
    
    
    public double getValueMin()
    {
        return this.valueMin;
    }
    
    
    public void setValueMin(double valueMin)
    {
        this.valueMin = valueMin;
    }
    
    public double getValueMax()
    {
        return this.valueMax;
    }
    
    public void setValueMax(double valueMax)
    {
        this.valueMax = valueMax;
    }
    
    public double getValueMean()
    {
        return this.valueMean;
    }
    
    public void setValueMean(double valueMean)
    {
        this.valueMean = valueMean;
    }
    
    public double getStd() {
		return std;
	}

	public void setStd(double std) {
		this.std = std;
	}

	public String getUnit()
    {
        return this.unit;
    }
    
    public void setUnit(String unit)
    {
        this.unit = unit;
    }
    

	public static List<Component> findComponentByParameterId(String parameterId)
    {
        // TODO : implement
        return null;
    }

	public void ejbLoad() {
		System.out.println("!!!!!!!!!!!! ejbLoad called !!!!!!!");
	}
}
