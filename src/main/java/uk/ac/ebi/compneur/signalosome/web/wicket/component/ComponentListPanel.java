package uk.ac.ebi.compneur.signalosome.web.wicket.component;


import java.util.ArrayList;
import java.util.List;

import net.databinder.models.hib.CriteriaFilterAndSort;

import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilteredAbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.GoAndClearFilter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import uk.ac.ebi.compneur.signalosome.data.Component;

/**
 * @author Anna Zhukova
 * @author rodrigue
 * 
 */
public class ComponentListPanel extends Panel 
{
	public ComponentListPanel(String containerId, List<Component> elements) 
	{
		this(containerId, "Components", elements);
	}
	
	public ComponentListPanel(String containerId, String panelTitle, List<Component> elements) 
	{
        super(containerId);
        // title
        add(new Label("componentTitle", panelTitle));

        // list of columns allows table to render itself without any markup from us
        List<IColumn<Component>> columns = new ArrayList<IColumn<Component>>();
        
        IModel<String> actionsModel = new Model<String>("Actions");
        
        columns.add(new FilteredAbstractColumn<Component>(actionsModel) { // use "" for no column header

                    public void populateItem(Item cellItem, String elementId, IModel rowModel) 
                    {
                        cellItem.add(new AnnotatedLinkPanel(elementId, rowModel));
                    }

                    // return the go-and-clear filter for the filter toolbar
                    public org.apache.wicket.Component getFilter(String id, FilterForm form) {
                        return new GoAndClearFilter(id, form);
                    }
                });
        columns.add(new TextFilteredPropertyColumn<Component, String>(new Model<String>("Id"), "id", "id"));
        columns.add(new TextFilteredPropertyColumn<Component, String>(new Model<String>("Name"), "name", "name"));
        columns.add(new TextFilteredPropertyColumn<Component, String>(new Model<String>("UserId"), "userId", "userId"));

        
        CriteriaFilterAndSort builder = new CriteriaFilterAndSort(new Component(), "name", true, false);
        
        FilterForm componentForm = new FilterForm("componentForm", builder);
        IDataProvider<Component> provider = new ListDataProvider<Component>(elements);

        DataTable<Component> table = new DataTable<Component>("components", columns, provider, 50) {
            @Override
            protected OddEvenItem<Component> newRowItem(String id, int index, IModel<Component> model) {
                return new OddEvenItem<Component>(id, index, model);
            }
        };
        table.addTopToolbar(new AjaxNavigationToolbar(table));
        table.addTopToolbar(new AjaxFallbackHeadersToolbar(table, builder));
        table.addBottomToolbar(new AjaxNavigationToolbar(table));
        componentForm.add(table);
        add(componentForm);
    }
}

