package uk.ac.ebi.compneur.signalosome.web;


import net.databinder.hib.DataApplication;
import net.databinder.hib.DataRequestCycle;

import org.apache.wicket.IRequestCycleProvider;
import org.apache.wicket.Page;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.cycle.RequestCycleContext;
import org.hibernate.cfg.AnnotationConfiguration;

import uk.ac.ebi.compneur.signalosome.data.User;


/**
 * Web Application base class
 */
public class SignalosomeApplication extends DataApplication
{
    
    /**
     * Constructor.
     */
    public SignalosomeApplication()
    {
    	// nothing
    }
    
    
    @Override
    protected void init()
    {
        super.init();
        getMarkupSettings().setStripWicketTags(true);   // remove all the "wicket:" before all tags used by Wicket
        mountPage("/search", Search.class);
        mountPage("/components", ComponentsPager.class);
        mountPage("/components/table", BrowseComponents.class);
        mountPage("/parameters", ParametersPager.class);
        mountPage("/interactions", InteractionsPager.class);
        mountPage("/component", ComponentEditPage.class);
        mountPage("/interaction", InteractionEditPage.class);
        mountPage("/parameter", ParameterEditPage.class);  
        mountPage("/delete", DeletePage.class);
        
        // TODO : we could probably have a generic Pager class on Annotated Object instead of 3 ?
        
        // TODO : check this article (including comments) http://www.javalobby.org/java/forums/t68753.html
        // We should be able to have automatic URL like /component/componentId to display individual elements.
        
        // mount("/testNiceURL", PackageName.forClass(ComponentEditPage.class));

    	// TODO 
//    	public RequestCycle newRequestCycle(Request request, Response response) {
//    	return new DataRequestCycle(this, (WebRequest) request, response);
    //}

        setRequestCycleProvider(new IRequestCycleProvider() {
			
			@Override
			public RequestCycle get(RequestCycleContext context) 
			{
				return new DataRequestCycle(context);
			}
		});
    }
    
    
    /**
     * Home page
     */
    @Override
    public Class<? extends Page> getHomePage()
    {
        return FrontPage.class;
    }
    
    protected void configureHibernate(AnnotationConfiguration config)
    {
        java.net.URL url = getClass().getResource("/hibernate.cfg.xml");
        config.configure(url);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Annotated.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Component.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Comment.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Annotation.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Interaction.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Tag.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Parameter.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Participant.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Unit.class);
        config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.util.ParameterRelation.class);
        //config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.User.class);   // otherwise: Duplicate class/entity mapping error
        config.configure().buildSessionFactory(); 
        
        // super.configureHibernate(config);
    }
    
    
    /**
     * User class used for authentication purposes.
     */
	public Class<User> getUserClass()
	{
		return User.class;
	}


	public byte[] getSalt()
	{
		return "This is the S1gnal0s0me project!".getBytes();
	}



}
