package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


/**
 * <p>Tag.
 * 
 * @author Camille Laibe <camille.laibe@ebi.ac.uk>
 * @version 20101109
 */
@Entity
@Table(name="tag")
@Audited
public class Tag extends Annotated implements Serializable
{
    @Transient
    private static final long serialVersionUID = -6027719459180980788L;
    
    @Column(name="definition")
    private String definition;
    
    
    /**
     * Default constructor.
     */
    public Tag()
    {
    	super();
        this.definition = null;
    }
    
    
    /**
     * Constructor.
     * @param name name or content of the tag
     * @param definition definition of the tag
     */
    public Tag(String name, String definition)
    {
    	super();
    	this.name = name;
        this.definition = definition;
    }
    
    
    /**
     * Returns a hash code value for the object.
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return (this.getId() + "-" + this.getName() + "(" + this.getDefinition() + ")").hashCode();
    }
    
    
    /**
     * Indicates whether some other object is "equal to" this one.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
        	return false;
        }
        if (getClass() != obj.getClass())
        {
        	return false;
        }
        Tag other = (Tag) obj;
        if (null == this.getId())
        {
            if (other.getId() != null)
            {
                return false;
            }
        }
        else if (!this.getId().equals(other.getId()))
        {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Getter
     * @return the definition
     */
    public String getDefinition()
    {
    	return this.definition;
    }
    
    /**
     * Setter
     * @param definition the definition to set
     */
    public void setDefinition(String definition)
    {
    	this.definition = definition;
    }
}
