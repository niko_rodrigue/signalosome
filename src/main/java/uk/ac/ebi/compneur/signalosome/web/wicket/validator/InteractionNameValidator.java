package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import uk.ac.ebi.compneur.signalosome.util.HibernateUtil;

/**
 * @author zhutchok
 *         Date: 04-May-2011
 *         Time: 16:37:55
 */
public class InteractionNameValidator extends UniqueNameValidator {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 
	 * 
     * @see UniqueNameValidator#isUnique(java.lang.String)
     */    
    protected boolean isUnique(String data) {
        return HibernateUtil.findInteractionByName(data) == null;
    }
}