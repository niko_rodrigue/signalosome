package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


/**
 * <p>Interaction
 * 
 * @author Nicolas Rodriguez and Camille Laibe
 * @version 20101109
 */
@Entity
@Table(name="interaction")
@Audited
public class Interaction extends Annotated implements Serializable
{
    @Transient
    private static final long serialVersionUID = -8594380698667503310L;
    
    @OneToMany(mappedBy="interaction")
    private List<Participant> participants = new ArrayList<Participant>();
    
    @OneToMany
    @JoinTable(name="param_rel",
               joinColumns = @JoinColumn( name="external_id"),
               inverseJoinColumns = @JoinColumn( name="parameter_id"))
    private List<Parameter> parameters = new ArrayList<Parameter>();

    /**
     * Default constructor.
     */
    public Interaction()
    {
        super();
    }
    
    
    /**
     * Constructor.
     * 
     * @param name
     */
    public Interaction(String name)
    {
        this.name = name;
    }
    
    /**
     * Constructor
     * 
     * @param id
     * @param name
     */
    public Interaction(String id, String name)
    {
        this.id = id;
        this.name = name;
    }
    
    
    /**
     * Getter
     * @return the participants
     */
    public List<Participant> getParticipants()
    {
        return this.participants;
    }
    
    
    /**
     * Setter
     * @param participants the participants to set
     */
    public void setParticipants(List<Participant> participants)
    {
        this.participants = participants;
    }
    
    
    /**
     * Adds a new participant.
     * 
     * @param participant
     */
    public void addParticipant(Participant participant)
    {
        if (null == this.participants) {
            this.participants = new ArrayList<Participant>();
        }
        
        this.participants.add(participant);
    }


	/**
	 * @param parameters
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}


	/**
	 * @return
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}
	
    /**
     * Adds a new parameter.
     * 
     * @param parameter
     */
    public void addParameter(Parameter parameter)
    {
        if (null == this.parameters) {
            this.parameters = new ArrayList<Parameter>();
        }
        
        this.parameters.add(parameter);
    }

    public List<Interaction> findInteractionByName() {
    	
    	
    	
    	return null;
    }
    
}
