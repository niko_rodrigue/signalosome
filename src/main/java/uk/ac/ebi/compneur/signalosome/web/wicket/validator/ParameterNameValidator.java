package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import uk.ac.ebi.compneur.signalosome.util.HibernateUtil;

/**
 * @author zhutchok
 *         Date: 04-May-2011
 *         Time: 16:50:37
 */
public class ParameterNameValidator extends UniqueNameValidator {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    protected boolean isUnique(String data) {    	
        return HibernateUtil.findParameterByName(data) == null;
    }
}