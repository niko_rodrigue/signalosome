package uk.ac.ebi.compneur.signalosome.web;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.IPageLink;
import org.apache.wicket.markup.html.panel.Panel;

public class ActionsPanel extends Panel
{

	public ActionsPanel(String wicketid, IPageLink editLink, IPageLink deleteLink)
	{
		super(wicketid);

		add(new BookmarkablePageLink<BasePage>("editLink", editLink.getPageIdentity()));
		add(new BookmarkablePageLink<BasePage>("deleteLink", deleteLink.getPageIdentity()));
	}
}