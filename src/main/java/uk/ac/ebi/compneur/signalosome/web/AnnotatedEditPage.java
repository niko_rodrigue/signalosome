package uk.ac.ebi.compneur.signalosome.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.databinder.auth.components.hib.DataSignInPage;
import net.databinder.auth.components.hib.DataUserStatusPanel;
import net.databinder.auth.hib.AuthDataSession;
import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.IResourceStream;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Comment;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.User;
import uk.ac.ebi.compneur.signalosome.util.ByteArrayResourceStream;
import uk.ac.ebi.compneur.signalosome.util.DataTypeNameComparator;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.DataTypeValidator;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.PatternValidator;
import uk.ac.ebi.export.sbml.SBMLExport;
import uk.ac.ebi.miriam.db.DataType;
import uk.ac.ebi.miriam.db.MIRIAMDatabase;
import uk.ac.ebi.miriam.db.Tag;
import uk.ac.ebi.miriam.lib.MiriamLink;


@AuthorizeInstantiation(Roles.USER)
public class AnnotatedEditPage extends BasePage
{
	private Annotated annotated;
	private TextArea<String> comments_text_area;
	private TextField<String> annotation_identifier;
	private DropDownChoice<String> qualifer_dropdown;
	private DropDownChoice<DataType> urn_dropdown;
    private SBMLExport exporter;
    private File outFile;
    byte[] outFileBytes = null;

	// drop down list for qualifiers
	// TODO - populate this list from MIRIAM
	private static List<String> QUALIFIERS = Arrays.asList(new String[] { "encodes", "hasPart", "hasProperty",
			"hasVersion", "is", "isDescribedBy", "isEncodedBy", "isHomolgTo", "isPartOf", "isPropertyOf", 
			"isVersionOf", "occursIn" });
	
	private ArrayList<DataType> datatypes;

	DataUserStatusPanel statusPanel;
	
	
	protected void init()
	{
		// sign in/out links
		statusPanel = new DataUserStatusPanel("userStatus")
		{
			private static final long serialVersionUID = 2277359067881440512L;
			
			@Override
			protected Link<AnnotatedEditPage> getSignInLink(String id)
			{
				return new Link<AnnotatedEditPage>(id)
				{
					@Override
					public void onClick()
					{
						setResponsePage(new DataSignInPage(new DataSignInPage.ReturnPage()
						{
							public Page get()
							{
								return new AnnotatedEditPage(null, null);   // I don't know what to put as parameters
							}
						}));
					}
					@Override
					public boolean isVisible()
					{
						return !getAuthSession().isSignedIn();
					}
				};
			}
		};
	}
	
	
	public AnnotatedEditPage(final Page backPage, HibernateObjectModel<? extends Annotated> model)
	{
		// Getting the Annotated Object from the HibernateObjectModel
		annotated = model.getObject();
		
		datatypes = new ArrayList<DataType>();
		for (DataType dataType: MIRIAMDatabase.instance.getResources())
		{
			
			uk.ac.ebi.miriam.db.Annotation SBMLannotation = null;
			for (uk.ac.ebi.miriam.db.Annotation annotation: dataType.getAnnotations())
			{
				if (annotation.getFormat().equals("SBML"))
				{
					SBMLannotation = annotation;
					break;
				}
			}
			
			if (SBMLannotation != null)
			{
				boolean test = false;
				for (Tag tag : SBMLannotation.getTags())
				{
					if (tag.getName().equals("species") && annotated instanceof Component)
					{
						test = true;
					}
					else if (tag.getName().equals("reaction") && annotated instanceof Interaction)
					{
						test = true;
					}
					else if (tag.getName().equals("parameter") && annotated instanceof Parameter)
					{
						test = true;
					}
				}
				if (test)
				{
					datatypes.add(dataType);
				}
			}
		}
		
		Collections.sort(datatypes, new DataTypeNameComparator());
		
		initAnnotationForm(model);
		initCommentForm(model);
	    
	}

	
	/**
	 * Initializes all the components of the Annotation Form.
	 * 
	 * @param model
	 */
	private void initAnnotationForm(HibernateObjectModel<? extends Annotated> model) {

		// add a header for the annotation section
		add(new Label("subtitle1", "Annotations"));
		
		// creates list of existing annotations
		List<Annotation> annotations = annotated.getAnnotations();
		
		ListView<Annotation> list_of_annotations = new ListView<Annotation>("annotationList", annotations) 
		{
			private static final long serialVersionUID = -4411672154046439813L;
			
			@Override
			protected void populateItem(ListItem<Annotation> item) 
			{
				Annotation ann = item.getModelObject();
				//item.add(new Label("annot_string", ann.toString()));
				MiriamLink miriam = new MiriamLink();   // TODO: update that with the disconnected library
				String datatype = miriam.getName(ann.getDataTypeUrn());
				String[] links = miriam.getLocations(ann.toString());
				ExternalLink annoLink = null;
				if ((null != links) && (links.length > 0))
				{
					annoLink = new ExternalLink("link_to_anno", links[0], ann.getMiriamId() + " (" + datatype + ")");
					annoLink.add(new AttributeAppender("title", new Model<String>("Link to: " + ann), ""));   // sets the 'title' attribute of the href 
				}
				else   // invalid MIRIAM URI
				{
					annoLink = new ExternalLink("link_to_anno", "http://www.ebi.ac.uk/miriam/", ann.getMiriamId() + " (" + datatype + ")");
					annoLink.add(new AttributeAppender("title", new Model<String>("WARNING: invalid MIRIAM URI (" + ann + ")!"), ""));   // sets the 'title' attribute of the href 
				}
				//annoLink.add(new Label ("identifier", ann.getMiriamId() + " (" + ann.getDataType() + ")"));
				item.add(annoLink);
				item.add(new Label("qualifier", ann.getQualifier()));
				item.add(new Link<Annotation>("del_anno", item.getModel())
						{
					public void onClick()
					{
						Annotation obj = getModelObject();
						setResponsePage(new DeleteAnnotationPage(obj));
					}
						});
			}
		};
		// list directly added to the page
		add(list_of_annotations);

		// creating the annotation form
		@SuppressWarnings({ "unchecked", "rawtypes" })
		final DataForm annotationForm = new DataForm("annotationForm", model) {
			
			protected void onSubmit()
			{
				
				// logger.debug("AnnotatedEditPage : main onSubmit called !!");
				
			}
		};
		add(annotationForm);				

		qualifer_dropdown = new DropDownChoice<String>("qualifierdropdown", new Model<String>("is"), QUALIFIERS);
		annotationForm.add(qualifer_dropdown);

		// add a search/auto-complete for the data types ??

		ChoiceRenderer<DataType> renderer_dropdown = new ChoiceRenderer<DataType>("name", "URN");
		urn_dropdown = new DropDownChoice<DataType>("urndropdown", new Model<DataType>(), 
				datatypes, renderer_dropdown);
		urn_dropdown.add(new DataTypeValidator());
		urn_dropdown.setRequired(true);
		annotationForm.add(urn_dropdown);

		annotation_identifier = new TextField<String>("anno_id", new Model<String>(""));
		// this validator does not work

		annotation_identifier.add(new PatternValidator(urn_dropdown, datatypes));
		
		annotation_identifier.setRequired(true);
		
		annotationForm.add(annotation_identifier);

		annotationForm.add(new Button("submit_annotation")
		{
			private static final long serialVersionUID = 4566895833012037816L;
			
			public void onSubmit()
			{
				// logger.debug("AnnotatedEditPage : addAnnotation.onSubmit called !!");
				
				Session session = Databinder.getHibernateSession();
				session.getTransaction();
				
				Annotation annotation = new Annotation();

				annotation.setExternalId(annotated.getId());
				// TODO : HACK until authentication works again annotation.setUserId(getLoggedUser().getUsername());
				annotation.setUserId("martinaf");
				
				if (annotated instanceof Component)
				{
					annotation.setExternalType("component");
				}
				else if (annotated instanceof Interaction)
				{
					annotation.setExternalType("interaction");        	
				}
				else if (annotated instanceof Parameter)
				{
					annotation.setExternalType("parameter");
				}

				// Set the qualifier
				annotation.setQualifier(QUALIFIERS.get(Integer.parseInt(qualifer_dropdown.getValue()))) ;

				// Set urn of annotation to add
				// Need to build this from the information given
				String miriamDatatypeURX = urn_dropdown.getValue();
				String miriamIdentifier = annotation_identifier.getValue();
				
				// retrieve the pattern associated
				
				try
				{
					miriamIdentifier = URLEncoder.encode(miriamIdentifier, "UTF-8");
				}
				catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}

				annotation.setUrn(miriamDatatypeURX + ":" + miriamIdentifier);

				// logger.debug("AnnotatedEditPage : addAnnotation.onSubmit : annotation about to be saved !!");
				
				getSession().info(String.format("An annotation has been added to %s", annotated.getName()));

				// logger.debug("AnnotatedEditPage : addAnnotation.onSubmit : after printing the info String !!");

				session.saveOrUpdate(annotation);
				annotated.addAnnotation(annotation);

				session.flush();
				session.getTransaction().commit();
				session.disconnect();
			}
		});

	}
	
	
	private void initCommentForm(HibernateObjectModel<? extends Annotated> model) {

		@SuppressWarnings({ "rawtypes", "unchecked" })
		final DataForm commentForm = new DataForm("commentForm", model);
		add(commentForm);
		
		// add a header for comments section
		commentForm.add(new Label("subtitle", "Comments"));
		
		// creates list of existing comments
		List<Comment> comments = annotated.getComments();
		
		final ListView<Comment> list_of_comments = new ListView<Comment>("commentList", comments) 
		{
			private static final long serialVersionUID = 2277093138808992452L;
			
			@Override
			protected void populateItem(ListItem<Comment> item) 
			{
				Comment c = item.getModelObject();
				String date = c.getDateString();
				if (date.length() > 0)
				{
					item.add(new Label("datestring", date));
				}
				else
				{
					item.add(new Label("datestring", "11-11-10"));
				}
				item.add(new Label("user", c.getUser()));
				item.add(new MultiLineLabel("comment", c.getComment()));
			}
		};
		// list directly added to the page
		commentForm.add(list_of_comments);
		
		comments_text_area = new TextArea<String>("new_comment", new Model<String>(""));
		commentForm.add(comments_text_area);
		
		commentForm.add(new Button("submit_comment")
		{
			private static final long serialVersionUID = 9180098168152431173L;
			
			public void onSubmit()
			{
				// not empty comment
				if ((null != comments_text_area.getValue()) && (!comments_text_area.getValue().matches("\\s*")))
				{
					// add comment to list of comments
					Comment comment = new Comment();
					comment.setExternalId(annotated.getId());
					
					if (annotated instanceof Component)
					{
						comment.setExternalType("component");
					}
					else if (annotated instanceof Interaction)
					{
						comment.setExternalType("interaction");        	
					}
					else if (annotated instanceof Parameter)
					{
						comment.setExternalType("parameter");
					}
					
					// set date of comment to add
					comment.setDate(new Date());
					
					// set user currently logged in
					// TODO : HACK until authentication works again annotation.setUserId(getLoggedUser().getUsername());
					comment.setUser("martinaf");
					
					comment.setComment(comments_text_area.getValue());
	
					getSession().info(String.format("A new comment has been posted for %s", annotated.getName()));
	
					Session session = Databinder.getHibernateSession();
					session.saveOrUpdate(comment);
					annotated.addComment(comment);
	
					session.flush();
					session.getTransaction().commit();
					session.disconnect();
				}
				else   // empty comment
				{
					// TODO: display a nice explanation to the user 
				}
			}
		}.setDefaultFormProcessing(false));

		initExportButton(commentForm);

	}

	
	private void initExportButton(@SuppressWarnings("rawtypes") DataForm form) {
		
		// Adding the export SBML button
	    form.add(new Button("export")
	    {
			private static final long serialVersionUID = 1L;
	
			public void onSubmit()
			{
				outFile = null;
				try
				{
				  outFile = File.createTempFile("temp", ".xml");
				}
				catch (IOException e)
				{
				  System.err.println("Could not create temp file"); 
				}
			    
				if (outFile != null)
				{
					exporter = new SBMLExport() ;
					exporter.writeAsSBMLModel(annotated, outFile);
				}
				// the outFileBytes being passed to the constructor was NULL
				// needs to be set somehow
				// this is probably not the way to do it
				long length = outFile.length();
				outFileBytes = new byte[(int)length];
		
				// Read in the bytes
				try
				{
					InputStream is = new FileInputStream(outFile);
					int offset = 0;
					int numRead = 0;
					while (offset < outFileBytes.length
						   && (numRead = is.read(outFileBytes, offset, outFileBytes.length - offset)) >= 0)
					{
						offset += numRead;
					}

					// Close the input stream
					is.close();

					// Ensure all the bytes have been read in
					if (offset < outFileBytes.length)
					{
						throw new IOException("Could not completely read file " + outFile.getName());
					}

				}
				catch (IOException e)
				{
					System.err.println("Could not create temp file for SBML export!");
				}

    			IResourceStream resourceStream = new ByteArrayResourceStream(outFileBytes, "application/sbml+xml");
    			
    			getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStream, "Signalosome.xml"));
    			
			}
	    }.setDefaultFormProcessing(false));

	}
	
	
	/**
	 * Enables or disable the add comment and add annotation components
	 * 
	 * @param enabled
	 */
	@SuppressWarnings("unchecked")
	protected void setMetadataEnabled(boolean enabled)
	{
		// get the Add annotation, Add comment and export SBML button and render them unusable.
		// get the 'Add Annotation' Button
		org.apache.wicket.Component addCommentButton = getPage().get("commentForm:submit_comment");
		((Button) addCommentButton).setEnabled(enabled);
		org.apache.wicket.Component newCommentTextArea = getPage().get("commentForm:new_comment");
		((TextArea<String>) newCommentTextArea).setEnabled(enabled);
		
		DropDownChoice<String> qualiferDropdown = (DropDownChoice<String>) getPage().get("annotationForm:qualifierdropdown");
		qualiferDropdown.setEnabled(enabled);

		DropDownChoice<String> urnDropdown = (DropDownChoice<String>) getPage().get("annotationForm:urndropdown");
		urnDropdown.setEnabled(enabled);

		TextField<String> annotationId = (TextField<String>) getPage().get("annotationForm:anno_id");
		annotationId.setEnabled(enabled);
		
		Button submitAnnoButton = (Button) getPage().get("annotationForm:submit_annotation");
		submitAnnoButton.setEnabled(enabled);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void setAnnotatedObject(Annotated newAnnotated, DataForm form)
	{
		Session session = Databinder.getHibernateSession();
		session.saveOrUpdate(newAnnotated);
		session.flush();
		session.getTransaction().commit();
		session.disconnect();
		form.clearPersistentObject();
		form.setModelObject(newAnnotated);
		
		// We need to empty the comments lists and annotations on the AnnotatedEditPage
		
		// get the 'commentList' component
		org.apache.wicket.Component commentListView = getPage().get("commentForm:commentList");
		// get the 'annotationList' component
		org.apache.wicket.Component annotationListView = getPage().get("annotationList");
		
		commentListView.setDefaultModelObject(newAnnotated.getComments());
		annotationListView.setDefaultModelObject(newAnnotated.getAnnotations());
	}
	
	
	/**
	 * Checks if the user is currently logged in.
	 * @return
	 */
	protected static boolean isSignedIn()
	{
		return AuthDataSession.get().isSignedIn();
	}
	
	/**
	 * Retrieves the user currently logged.
	 * @return
	 */
	protected static User getLoggedUser()
	{
		return (User) AuthDataSession.get().getUser();
	}
}
