package uk.ac.ebi.compneur.signalosome.web;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Participant;

public class DeleteParticipantPage extends BasePage
{
	public DeleteParticipantPage(final Page backPage, HibernateObjectModel<Participant> model)
	{
		super(model);

		Participant participant = model.getObject();
		
		add(new Label("name", String.format("%s %s", participant.getId(), participant.getComponent().getName())));

		DataForm<Participant> form = new DataForm<Participant>("confirmForm", model)
		{
			protected void onSubmit()
			{
				Participant participant = getModelObject();
				Session session = Databinder.getHibernateSession();
				session.delete(participant);
				session.flush();
				session.getTransaction().commit();
				getSession().info(String.format("Deleted %s %s,", participant.getId(), participant.getComponent().getName()));
				setResponsePage(backPage);
			}
		};

		form.add(new Button("cancel")
		{
			public void onSubmit()
			{
				setResponsePage(backPage);
			}
		}.setDefaultFormProcessing(false));

		add(form);
	}
}