package uk.ac.ebi.compneur.signalosome.web;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import net.databinder.models.hib.HibernateListModel;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.IResourceStream;

import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.util.ByteArrayResourceStream;
import uk.ac.ebi.export.sbml.SBMLExport;
//import org.apache.log4j.Logger;


/**
 * SBMLExport page
 */
public class SbmlFullExport extends Link
{
	private static final long serialVersionUID = -7149214634245156713L;
	//private Logger logger = Logger.getLogger(SbmlFullExport.class);
	
	private SBMLExport exporter;
	private HibernateListModel<Component> componentListModel;
	private HibernateListModel<Parameter> parameterListModel;
	private HibernateListModel<Interaction> interactionListModel;
	private TextField<String> filename;
	private File outFile;
    byte[] outFileBytes = null;
    
    
    /**
     * Constructor
     */
    public SbmlFullExport(String id)
    {
    	super(id);
    }
    
    
    public void onClick()
    {
	    // list all Component
	    componentListModel = new HibernateListModel<Component>(Component.class);
	    // list all Parameters
	    parameterListModel = new HibernateListModel<Parameter>(Parameter.class);
	    // list all Interactions
	    interactionListModel = new HibernateListModel<Interaction>(Interaction.class);
	    
	    outFile = null;
	    try
	    {
	    	outFile = File.createTempFile("temp", ".xml");
	    }
	    catch (IOException e)
	    {
	    	//logger.error("Could not create temp file for SBML export!");
	    	System.err.println("Could not create temp file for SBML export!");
	    }
	    
	    if (outFile != null)
	    {
	      // generates SBML export
	      // NOTE: the getValue function fails if you try and change it
	      // I'll come back to this
	      exporter = new SBMLExport();
	      exporter.writeSBML(componentListModel, parameterListModel, interactionListModel, outFile);
	    }

        // the outFileBytes being passed to the constructor was NULL
		// needs to be set somehow
		// this is probably not the way to do it
		long length = outFile.length();
		outFileBytes = new byte[(int)length];
		
		// Read in the bytes
		try
		{
			InputStream is = new FileInputStream(outFile);
			int offset = 0;
			int numRead = 0;
			while (offset < outFileBytes.length
				   && (numRead = is.read(outFileBytes, offset, outFileBytes.length - offset)) >= 0)
			{
				offset += numRead;
			}

			// Close the input stream
			is.close();

			// Ensure all the bytes have been read in
			if (offset < outFileBytes.length)
			{
				throw new IOException("Could not completely read file " + outFile.getName());
			}

		}
		catch (IOException e)
		{
			System.err.println("Could not create temp file for SBML export!");
		}

    	IResourceStream resourceStream = new ByteArrayResourceStream(outFileBytes, "application/sbml+xml");
    	getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStream, "Signalosome.xml"));
    }
}
