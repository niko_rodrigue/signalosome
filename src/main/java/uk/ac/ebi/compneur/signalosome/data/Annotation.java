package uk.ac.ebi.compneur.signalosome.data;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;


/**
 * <p>Annotation.
 * 
 * @author Camille Laibe <camille.laibe@ebi.ac.uk>
 */
@Entity
@Table(name="annotation")
@Audited
public class Annotation implements Serializable
{
    @Id
    @GenericGenerator(name="id", strategy="uk.ac.ebi.compneur.signalosome.data.AnnotatedIdGenerator")
    @GeneratedValue(generator="id")
    private String id;
    private String qualifier;
    @Column(name="set_index")
    private int index;
    @Column(name = "miriam_uri")
    private String urn;   // MIRIAM full URI with datatype and id
    @Column(name="external_id")
    private String externalId;
    @Column(name = "external_type")
    private String externalType;    
    @Column(name = "user_id")
    private String userId;
    
    
    @Transient
    private static final long serialVersionUID = -6356033654089955186L;
    
    
    /**
     * Indicates whether some other object is "equal to" this one.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Annotation other = (Annotation) obj;
        if (null == this.getId())
        {
            if (other.getId() != null)
            {
                return false;
            }
        }
        else if (!this.getId().equals(other.getId()))
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns a hash code value for the object.
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return (this.getId() + "_" + this.getQualifier() + "_" + this.getUrn()).hashCode();
    }
    
    
    /**
     * Getter
     * @return qualifier
     */
    public String getQualifier()
    {
        return this.qualifier;
    }
    
	public void setExternalId(String externalId)
	{
		this.externalId = externalId;
	}
	
	public String getExternalId()
	{
		return this.externalId;
	}
  
    public String getExternalType()
    {
        return this.externalType;
    }
    
    public void setExternalType(String externalType)
    {
        this.externalType = externalType;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * Setter
     * @param qualifier
     */
    public void setQualifier(String qualifier)
    {
        this.qualifier = qualifier;
    }
    
    /**
     * Getter
     * @return URN
     */
    public String getUrn()
    {
        return urn;
    }
    
    /**
     * Setter
     * @param urn
     */
    public void setUrn(String urn)
    {
        this.urn = urn;
    }
    
    /**
     * Gets the decoded identifier part of the full annotation (URXX).
     * 
     * @return the decoded identifier part of the full annotation URXX.
     */
    public String getMiriamId()
    {
		int lastColonIndex = urn.lastIndexOf(":");
		String miriamId = urn.substring(lastColonIndex + 1);
		
		try
		{
			miriamId = URLDecoder.decode(miriamId, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		return miriamId;
    }
    
    /**
     * Retrieve the date type part of the URN
     */
    public String getDataTypeUrn()
    {
    	int lastColonIndex = urn.lastIndexOf(":");
    	String datatype = urn.substring(0, lastColonIndex);   // part after the initial "urn:miriam:"
    	
    	return datatype;
    }
    
    /**
     * Gets the URI part of the full annotation (URXX).
     * 
     * @return the URI part of the full annotation (URXX).
     */
    public String getMiriamURI()
    {
		int lastColonIndex = urn.lastIndexOf(":");
		String uri = urn;
		
		if (lastColonIndex != -1) {
			uri = urn.substring(0, lastColonIndex);
		} else {
			// There are some annotation like 'DOCQS' !!
			System.out.println("Annotation : getMiriamURI : urn = " + urn);
		}
		
		return uri;
    }
    
	@Override
    public String toString()
    {
		return this.urn;
    }
	
	/**
	 * Previously implemented in 'toString()'.
	 * 
	 * @return
	 */
	public String getFormattedUrn()
	{
		int lastColonIndex = urn.lastIndexOf(":");
		
		if (lastColonIndex == -1)
		{
			return urn;
		}
		String miriamId = urn.substring(lastColonIndex + 1);
		String uri = urn.substring(0, lastColonIndex);
		
		try
		{
			miriamId = URLDecoder.decode(miriamId, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
        return qualifier + " " + uri + " : " + miriamId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setIndex(int index) 
	{
		this.index = index;
	}
	
	public int getIndex() {
		return index;
	}
}
