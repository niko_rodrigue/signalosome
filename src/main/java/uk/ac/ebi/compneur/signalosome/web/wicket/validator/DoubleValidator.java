
package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

/**
 * Validator for checking if a given String is a valid double or not.
 * 
 */
public class DoubleValidator implements IValidator<String>
{

	/**
	 * Constructor .
	 * 
	 */
	public DoubleValidator()
	{
	}


	/** {@inheritDoc} */
	public void validate(IValidatable<String> validatable)
	{
		String value = validatable.getValue();
		
		try {
			Double.parseDouble(value);
		} catch(NumberFormatException e) {
			ValidationError error = new ValidationError();
			error.setMessage("You need to specify a valid double, " + value + " was given");
			validatable.error(error);			
		}
	}


}