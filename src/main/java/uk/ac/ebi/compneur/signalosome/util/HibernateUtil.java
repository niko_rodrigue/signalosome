package uk.ac.ebi.compneur.signalosome.util;

import java.util.ArrayList;
import java.util.List;

import net.databinder.hib.Databinder;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.context.internal.ManagedSessionContext;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;

public class HibernateUtil {

	
	// Use this commented block when outside of the webApp, you also have to change getHibernateSession()
	
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
        	
        	// TODO : check what to do for the AnnotationConfiguration
        	
            // Create the SessionFactory from hibernate.cfg.xml
        	AnnotationConfiguration config = new AnnotationConfiguration();

        	config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Annotated.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Component.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Comment.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Annotation.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Interaction.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Tag.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Parameter.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Participant.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.User.class);
            config.addAnnotatedClass(uk.ac.ebi.compneur.signalosome.data.Unit.class);
            return config.configure().buildSessionFactory(); 

        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
	
    public static Session getHibernateSession() {
    	
    	// Use this line when outside of the webApp
    	// Session session = HibernateUtil.getSessionFactory().getCurrentSession();

//		SessionFactory factory = Databinder.getHibernateSessionFactory();
//		
//		System.out.println(ManagedSessionContext.hasBind(factory));
//		
//		StatelessSession statelessSession = factory.openStatelessSession(); // TODO : check what is a StatelessSassion to see if it is better to use here
		
    	return Databinder.getHibernateSession();
    }

	public static Component findComponentById(String componentId) {

		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

        Component component = (Component) session.get(Component.class, componentId); 
        
        // session.disconnect();
        
		return component;
	}


	public static Component findComponentByName(String componentName) {

		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

        Component component = null;
        
        Query query = session.createQuery("from Component where name = '" + componentName + "'");
        	
        if (query.list().size() > 0) {
        	component = (Component) query.list().get(0);
        }
        
        // session.disconnect();
        
		return component;
	}

	public static List<Component> findComponentsByAnnotation(String uri) {
		// ?? Useful for the search ? Could we use HibernateSearch ??
		
		return null;
	}

	public static Interaction findInteractionById(String interactionId) {

		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

        Interaction interaction = (Interaction) session.get(Interaction.class, interactionId); 
        	
        // session.disconnect();
        
		return interaction;
	}

	public static Interaction findInteractionByName(String interactionName) {

		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();
        
        Interaction interaction = null;
        
        Query query = session.createQuery("from Interaction where name = '" + interactionName + "'");
        	
        if (query.list().size() > 0) {
        	interaction = (Interaction) query.list().get(0);
        }
        
        // session.disconnect();
        
		return interaction;
	}

	
	public static Parameter findParameterByName(String parameterName) {

		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

        Parameter parameter = null;
        
        Query query = session.createQuery("from Parameter where name = '" + parameterName + "'");
        	
        if (query.list().size() > 0) {
        	parameter = (Parameter) query.list().get(0);
        }
        
        // session.disconnect();
        
		return parameter;
	}

	public static List<Component> searchComponents(String searchQuery) 
	{
		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

		// getting rid of any '%' as we are adding them all the time in the query below
        searchQuery = searchQuery.replace("%", "");

        List<Component> matchedComponents = new ArrayList<Component>();
        List<String> componentIds = new ArrayList<String>();
        
        Query query = session.createQuery("from Component where name like '%" + searchQuery + "%'");
        int nbMatch = query.list().size();
        
        if (nbMatch > 0) {
        	for (Object c : query.list()) {
            	matchedComponents.add((Component) c);
            	componentIds.add(((Component) c).getId());
        	}
        }
        
        
        query = session.createQuery("from Annotation where miriam_uri like '%" + searchQuery + "%' AND external_type = 'component'");
        nbMatch = query.list().size();
        
        if (nbMatch > 0) 
        {
        	for (Object obj : query.list()) 
        	{
        		Annotation anno = (Annotation) obj;
        		
        		if (! componentIds.contains(anno.getExternalId()))
        		{
            		Component component = (Component) session.get(Component.class, anno.getExternalId());
            		
            		if (component != null) 
            		{
            			matchedComponents.add(component);
            		}
            		else
            		{
            			System.out.println("Error : the annotation '" + anno.getId() + "' is referencing a non existing Component !! (" + anno.getExternalId() + ")");
            		}
        		}
        	}
        }
        
		return matchedComponents;
	}

	public static List<Interaction> searchInteractions(String searchQuery) 
	{
		Session session = HibernateUtil.getHibernateSession();
        // session.beginTransaction();

		// getting rid of any '%' as we are adding them all the time in the query below
        searchQuery = searchQuery.replace("%", "");

        List<Interaction> matchedInteractions = new ArrayList<Interaction>();
        List<String> interactionIds = new ArrayList<String>();
        
        Query query = session.createQuery("from Interaction where name like '%" + searchQuery + "%'");
        int nbMatch = query.list().size();
        
        if (nbMatch > 0) 
        {
        	for (Object c : query.list()) 
        	{
            	matchedInteractions.add((Interaction) c);
            	interactionIds.add(((Interaction) c).getId());
        	}
        }
        
        
        query = session.createQuery("from Annotation where miriam_uri like '%" + searchQuery + "%' AND external_type = 'interaction'");
        nbMatch = query.list().size();
        
        if (nbMatch > 0) 
        {
        	for (Object obj : query.list()) 
        	{
        		Annotation anno = (Annotation) obj;
        		
        		if (! interactionIds.contains(anno.getExternalId()))
        		{
        			Interaction interaction = (Interaction) session.get(Interaction.class, anno.getExternalId());
            		
            		if (interaction != null) 
            		{
            			matchedInteractions.add(interaction);
            		}
            		else
            		{
            			System.out.println("Error : the annotation '" + anno.getId() + "' is referencing a non existing Interaction !! (" + anno.getExternalId() + ")");
            		}
        		}
        	}
        }
        
		return matchedInteractions;
	}

}