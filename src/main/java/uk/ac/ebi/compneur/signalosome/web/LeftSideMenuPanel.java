package uk.ac.ebi.compneur.signalosome.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

public class LeftSideMenuPanel extends Panel
{
	/**
     * Constructor
     */
    public LeftSideMenuPanel(String wicketId)
    {
    	super(wicketId);
    	
    	add(new BookmarkablePageLink<WebPage>("homeLSMPLink", FrontPage.class));
        add(new BookmarkablePageLink<WebPage>("componentsLSMPLink", ComponentsPager.class));
        add(new BookmarkablePageLink<WebPage>("parametersLSMPLink", ParametersPager.class));
        add(new BookmarkablePageLink<WebPage>("interactionsLSMPLink", InteractionsPager.class));
	}
}
