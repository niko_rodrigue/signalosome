package uk.ac.ebi.compneur.signalosome.web.wicket.model;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.persistence.Transient;


import org.apache.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.model.IModel;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.Participant;
import uk.ac.ebi.ontology.sbo.SBO;

public class SBOAnnotationModel implements IModel<String> {

	@Transient private transient Logger logger = Logger.getLogger(this.getClass());
	
	private Annotated annotated;		
	private String sboId;
	private String sboName;

	public SBOAnnotationModel(Annotated annotated) {
		this.annotated = annotated;
		getObject(); // Call to initialize sboId and sboName if an SBO annotation exist.
	}

	/**
	 * Returns the SBO annotation of the underlying annotated object if it has one, null otherwise.
	 * 
	 * @return the SBO annotation of the underlying annotated object if it has one, null otherwise.
	 */
	public Annotation getSBOAnnotation() {
		return annotated.getSBOAnnotation();
	}

	public String getObject() {

		Annotation annotation = getSBOAnnotation();

		if (annotation != null) {

			sboId = annotation.getUrn().substring(annotation.getUrn().lastIndexOf(":") + 1);
			try {
				sboId = URLDecoder.decode(sboId, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			logger.debug("getObject : sboId = " + sboId);

			sboName = SBO.getTermName(sboId);

			return sboName;
		}

		return null;
	}

	public void setObject(String object) {

		Annotation annotation = getSBOAnnotation();

		// Set the sboId from the name.

		logger.debug("setObject : " + object);

		sboName = object.trim();
		sboId = SBO.getTermId(sboName);

		if (sboId == null && SBO.checkTerm(sboName)) {
			
			// The user entered directly an SBO ID
			sboId = sboName;
			
		} else if (sboId == null) { // What the user entered is incorrect, neither a valid SBO Id or name.
			return;
		}

		String sboIdEncoded = sboId;

		try {
			sboIdEncoded = URLEncoder.encode(sboId, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		logger.debug("setObject : " + sboIdEncoded + ". Annotation = " + annotation);

		if (annotation != null) {

			annotation.setUrn(Annotated.SBO_URN + ":" + sboIdEncoded);
			// TODO : HACK until authentication works again annotation.setUserId(AuthenticatedWebSession.get().getId()); // TODO : check if we correctly get the username
			annotation.setUserId("martinaf");

		} else {
			annotation = new Annotation();
			annotation.setExternalId(annotated.getId());
			// TODO : HACK until authentication works again annotation.setUserId(AuthenticatedWebSession.get().getId()); // TODO : check if we correctly get the username
			annotation.setUserId("martinaf");
			
			// TODO : could/should be done automatically. May be having methods like annotation.setExternalType(Annotated);
			if (annotated instanceof Component) {
				annotation.setExternalType("component");
			} else if (annotated instanceof Interaction) {
				annotation.setExternalType("interaction");        	
			} else if (annotated instanceof Parameter) {
				annotation.setExternalType("parameter");
			} else if (annotated instanceof Participant) {
				annotation.setExternalType("participant");
			} 

			annotation.setUrn(Annotated.SBO_URN + ":" + sboIdEncoded);
			annotation.setQualifier("is");
			
			annotated.addAnnotation(annotation);
		}
	}

	public void detach() {
	}

	@Override
	public String toString() {
		return sboName;
	}

	@Override
	public int hashCode() {
		return sboId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

}
