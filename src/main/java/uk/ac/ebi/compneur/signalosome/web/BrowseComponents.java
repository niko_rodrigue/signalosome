package uk.ac.ebi.compneur.signalosome.web;


import java.util.List;

import net.databinder.hib.DataApplication;
import net.databinder.models.hib.HibernateListModel;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import uk.ac.ebi.compneur.signalosome.data.Comment;
import uk.ac.ebi.compneur.signalosome.data.Component;


/**
 * Display all Components
 */
public class BrowseComponents extends BasePage
{
    public BrowseComponents()
    {
        // subtitle
        add(new Label("subtitle", "Components"));
        // general links
        add(new BookmarkablePageLink<BasePage>("link_intro", FrontPage.class));
        
        // lists all the Component
        ((DataApplication) getApplication()).getHibernateSessionFactory(null).getCurrentSession().beginTransaction();
        HibernateListModel<Component> componentListModel = new HibernateListModel<Component>(Component.class);
        
        add(new ListView<Component>("components", componentListModel)
        {
            @Override
            protected void populateItem(ListItem<Component> item)
            {
                Component component = item.getModelObject();
                item.add(new Label("name", component.getName()));
                List<Comment> comments = component.getComments();
                if ((null != comments) && (comments.size() > 0))
                {
                    item.add(new Label("comment", comments.get(0).getComment()));   // only retrieves the first comment!
                }
                else
                {
                    item.add(new Label("comment", ""));
                }
                // link to component's details
                PageParameters params = new PageParameters();
                params.add("cid", component.getId());
                BookmarkablePageLink<BasePage> link = new BookmarkablePageLink<BasePage>("link_component_details", DisplayComponent.class, params);
                link.add(new Label("id", component.getId()));
                item.add(link);
            }
        });
    }
}
