package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

/**
 * @author zhutchok
 * 
 */
public abstract class UniqueNameValidator implements IValidator<String>
{
    private String oldName = null;

    /** 
     * Validates a name. A {@link ValidationError} will be set
     * if the String given to validate is not unique.
     * 
     */
	public void validate(IValidatable<String> validatable)
	{
		String data = validatable.getValue();
		// System.out.println("UniqueNameValidator : name to validate = " + data);

		if (data != null && (!nameHasChanged(data) || isUnique(data))){
			return;
		} else {
			ValidationError error = new ValidationError();
			error.setMessage("The name already exists, please, specify an unique name.");
			validatable.error(error);
		}
	}

    /**
     * Returns true if the name has changed
     * 
     * @param name
     * 
     * @return true if the name has changed
     */
    private boolean nameHasChanged(String name) {
    	// name is case sensitive, so we are not using equalsIgnoreCase
    	// but may be we should have a way to check name that are quite similar
    	// or are annotated with the same annotations ??!
        return oldName == null || !oldName.trim().equals(name.trim());
    }

    /**
     * Returns true if the given name is unique.
     * 
     * @param name
     * 
     * @return true if the given name is unique.
     */
    protected abstract boolean isUnique(String name);

    /**
     * Sets the old name of the element, which is to be validated.
     * <br/>
     * For the new elements the old name should be null.
     * If the element is new, then the name is checked to be unique,
     * otherwise it's checked to be unique only if changed.
     * 
     * @param name The old element name or null if the element is new.
     */
    public void setOldName(String name) {
        oldName = name;
    }

    /**
     * Checks wether element is a new one
     * @return true, if element is new.
     */
    public boolean isNew() {
        return oldName == null;
    }
}

