
package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import java.util.ArrayList;
import java.util.Iterator;

import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import uk.ac.ebi.miriam.db.DataType;

/**
 * Validator for checking if a given identifier is valid Datatype is a valid integer or not.
 * 
 */
public class PatternValidator implements IValidator<String>
{

	@Transient private transient Logger logger = Logger.getLogger(PatternValidator.class);
	
	private DropDownChoice<DataType> data;
	private ArrayList<DataType> datatypeslist;

	/**
	 * Constructor
	 * 
	 */
	public PatternValidator(DropDownChoice<DataType> data, ArrayList<DataType> datatypeslist)
	{
		this.data = data;
		this.datatypeslist = datatypeslist;
	}

	
	/** {@inheritDoc} */
	public void validate(IValidatable<String> validatable)
	{
		String identifier = validatable.getValue();
		logger.debug("value of the identifier to valide = " + validatable.getValue());
		
		String dataTypeURN = data.getValue();
		
		if (dataTypeURN == null || dataTypeURN.length() == 0) {
			logger.debug("dataType URN was NULL ???");			

			return;
		}
		
		Iterator<DataType> it = datatypeslist.iterator();
		DataType datatype = null;
		Boolean match = false;
		
		while (it.hasNext())
		{
			DataType element = it.next();			
			
			if (dataTypeURN.equals(element.getURN())) {
				
				datatype = element;
				match = identifier.matches(element.getRegexp());
				logger.debug("reg exp of the datatype = " + element.getRegexp() );
				
				break;
			}
		}
		
		if (match){
			return;
		} else {
			ValidationError error = new ValidationError();
			error.setMessage("The given identifier "+ identifier +" does not match the given DataType id pattern : " + datatype.getName() + " idPattern = " + datatype.getRegexp());
			validatable.error(error);
			return;
		}
	}
}