package uk.ac.ebi.compneur.signalosome.util;

import java.util.Comparator;

import uk.ac.ebi.miriam.db.DataType;

/**
 * Compare two {@link DataType} Names ignoring the case.
 * 
 * @author jalowcki
 *
 */
public class DataTypeNameComparator implements Comparator<DataType>
{
	public int compare(DataType o1, DataType o2) 
	{
		String name1 = o1.getName().toUpperCase();
		String name2 = o2.getName().toUpperCase();
		return name1.compareTo(name2);
	}

}
