package uk.ac.ebi.compneur.signalosome.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;



public class BasePage extends WebPage
{
	public BasePage()
	{
		super();
		init();
	}
	
	public BasePage(IModel<?> model) {
		super(model);
		init();
	}

	public BasePage(PageParameters parameters) {
		super(parameters);
		init();
	}
	
	private void init() 
	{
		// add(new DataStyleLink("css"));
		add(new FeedbackPanel("feedback"));
//		add(new BookmarkablePageLink("homeLink", FrontPage.class));
//        add(new BookmarkablePageLink("componentLink", ComponentsPager.class));
//        add(new BookmarkablePageLink("searchLink", Search.class));

        // testing
        add(new LeftSideMenuPanel("leftSideMenuPanel"));

	}
}
