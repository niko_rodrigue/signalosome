package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


/**
 * <p>Participant
 * 
 * @author Nicolas Rodriguez and Camille Laibe
 * @version 20101109
 */
@Entity
@Table(name="participate")
@Audited
public class Participant extends Annotated implements Serializable
{
    @Transient
    private static final long serialVersionUID = 1344036870398724477L;
    
	@OneToOne
    @JoinColumn( name="component_id")
	private Component component;

	@ManyToOne
    @JoinColumn( name="interaction_id")
	private Interaction interaction;
	
	@Column(name="stoichiometry")
	private Double stoichiometry;
	
	@Column(name="is_left")
	private Boolean isLeft;
	
	
	public Participant()
	{
		// nothing
	}
	
	
	public Participant(String id)
	{
		this.id = id;
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Component getComponent()
	{
		return this.component;
	}
	
	public void setComponent(Component component)
	{
		this.component = component;
	}
	
	public Interaction getInteraction()
	{
		return this.interaction;
	}
	
	public void setInteraction(Interaction interaction)
	{
		this.interaction = interaction;
	}
	
	public double getStoichiometry()
	{
		return this.stoichiometry;
	}
	
	public void setStoichiometry(double stoichiometry)
	{
		this.stoichiometry = stoichiometry;
	}
	
	public boolean isLeft()
	{
		return this.isLeft;
	}
	
	public void setLeft(boolean isLeft)
	{
		this.isLeft = isLeft;
	}
}
