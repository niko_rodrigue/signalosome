package uk.ac.ebi.compneur.signalosome.web;


import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.request.mapper.parameter.PageParameters;


/**
 * Front page of the application.
 */
public class FrontPage extends BasePage
{
	/**
     * Constructor
     */
    public FrontPage()
    {
        // subtitle
        add(new Label("subtitle", "Introduction"));
        // links

        
        add(new BookmarkablePageLink<BasePage>("link_search_component", Search.class, new PageParameters().add("type", "component")));
        add(new BookmarkablePageLink<BasePage>("link_search_interaction", Search.class, new PageParameters().add("type", "interaction")));

        /*
        add(new BookmarkablePageLink<BasePage>("link_components", BrowseComponents.class));
        add(new BookmarkablePageLink<WebPage>("link_testing", Testing.class));
        add(new BookmarkablePageLink<BasePage>("link_niceComponents", ComponentsPager.class));
        add(new BookmarkablePageLink<WebPage>("link_searching", SearchComponent.class));
        add(new BookmarkablePageLink<BasePage>("link_search", Search.class));
        */
        //add(new BookmarkablePageLink<WebPage>("link_SBMLExport", SBMLExportPage.class));
        add(new SbmlFullExport("SBML_export"));
        
	}
}
