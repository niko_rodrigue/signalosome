package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


/**
 * <p>Component
 * 
 * @author Nicolas Rodriguez and Camille Laibe
 * @version 20101109
 */
@Entity
@Table(name="component")
@Audited
public class Component extends Annotated implements Serializable
{
    @Transient
    private static final long serialVersionUID = -4746172865597932180L;
    
    @OneToMany
    @JoinTable(name="param_rel",
               joinColumns = @JoinColumn( name="external_id"),
               inverseJoinColumns = @JoinColumn( name="parameter_id"))
    private List<Parameter> parameters = new ArrayList<Parameter>();

    // Annotation to be used, in case we don't want one collection to be audited
    // @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)       

    
    /**
     * Default constructor
     */
    public Component()
    {
        super();
    }
    
    /**
     * Constructor
     * @param id
     * @param name
     */
    public Component(String id, String name)
    {
        this.id = id;
        this.name = name;
    }
    
    
    public Component(String name)
    {
        this.name = name;
    }
    
	/**
	 * @param parameters
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}


	/**
	 * @return
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}
	
    /**
     * Adds a new parameter.
     * 
     * @param parameter
     */
    public void addParameter(Parameter parameter)
    {
        if (null == this.parameters) {
            this.parameters = new ArrayList<Parameter>();
        }
        
        this.parameters.add(parameter);
    }

    
    public static Component findComponentById(String componentId)
    {
        // TODO : implement
        return null;
    }
    
    public static List<Annotation> findAnnotationsByComponentId(String componentId)
    {
        // TODO : implement
        return null;
    }
    
    public static List<Tag> findTagsByComponentId(String componentId)
    {
        // TODO : implement
        return null;
    }
    
    public static List<Interaction> findInteractionsByComponentId(String componentId)
    {
        // TODO : implement
        return null;
    }
    
    public static List<Parameter> findParametersByComponentId(String componentId)
    {
        // TODO : implement
        return null;
    }
}
