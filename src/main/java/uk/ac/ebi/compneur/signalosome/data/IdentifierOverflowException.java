package uk.ac.ebi.compneur.signalosome.data;


/**
 * Custom exception to handle the generation of identifiers.
 * It extends "RuntimeException" instead of "Exception", in order to avoid to add "throws XXX" or try/catch blocks in each method which can throw this exception.
 * 
 * @author Camille Laibe
 * @version 20100616
 *
 */
public class IdentifierOverflowException extends RuntimeException
{
    private static final long serialVersionUID = -830377419255980066L;
    
    
    /**
     * Default constructor.
     */
    public IdentifierOverflowException()
    {
        // nothing here.
    }
    
    
    public IdentifierOverflowException(String s)
    {
        super(s);
    }
}
