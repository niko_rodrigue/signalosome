package uk.ac.ebi.compneur.signalosome.web;


import java.util.ArrayList;
import java.util.List;

import net.databinder.models.hib.CriteriaFilterAndSort;
import net.databinder.models.hib.HibernateListModel;
import net.databinder.models.hib.HibernateProvider;

import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilteredAbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.GoAndClearFilter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.AnnotatedLinkPanel;


/**
 * Displays paginated, filtered, and sorted Parameter objects.
 */
@SuppressWarnings("rawtypes")
public class ParametersPager extends BasePage
{
	// Cached list of Parameter ??
	HibernateListModel<Parameter> parameters = new HibernateListModel<Parameter>("from Parameter order by name");
	
	@SuppressWarnings("unchecked")
	public ParametersPager()
	{
		super();
		
		// list of columns allows table to render itself without any markup from us
		List<IColumn<Parameter>> columns = new ArrayList<IColumn<Parameter>>();
		
		columns.add(new FilteredAbstractColumn(new Model("Actions")) { // use "" for no column header
                    public void populateItem(Item cellItem, String parameterId, IModel rowModel)
                    {
                        cellItem.add(new AnnotatedLinkPanel(parameterId, rowModel));
                    }
					// return the go-and-clear filter for the filter toolbar
					public org.apache.wicket.Component getFilter(String id, FilterForm form)
					{
						return new GoAndClearFilter(id, form);
					}
                });
		
		columns.add(new TextFilteredPropertyColumn(new Model("Id"), "id", "id"));
		columns.add(new TextFilteredPropertyColumn(new Model("Name"), "name", "name"));
		columns.add(new TextFilteredPropertyColumn(new Model<String>("UserId"), "userId", "userId"));
		
		
		CriteriaFilterAndSort builder = new CriteriaFilterAndSort(new uk.ac.ebi.compneur.signalosome.data.Parameter(), "name", true, false);
		FilterForm form = new FilterForm("form", builder);
		HibernateProvider provider = new HibernateProvider(uk.ac.ebi.compneur.signalosome.data.Parameter.class, builder);
		provider.setWrapWithPropertyModel(false);
		
		DataTable table = new DataTable("parameters", columns, provider, 50) {
			@Override protected Item newRowItem(String id, int index, IModel model)
			{
				return new OddEvenItem(id, index, model);
			}
		};
		table.addTopToolbar(new AjaxNavigationToolbar(table));
		table.addTopToolbar(new FilterToolbar(table, form, builder));
		table.addTopToolbar(new AjaxFallbackHeadersToolbar(table, builder));
		table.addBottomToolbar(new AjaxNavigationToolbar(table));
		add(form.add(table));
	}
}
