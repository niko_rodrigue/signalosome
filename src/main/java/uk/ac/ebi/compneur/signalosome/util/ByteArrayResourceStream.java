package uk.ac.ebi.compneur.signalosome.util;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.apache.wicket.util.lang.Bytes;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.resource.ResourceStreamNotFoundException;
import org.apache.wicket.util.time.Time;


/**
 * Utility class for downloading dynamically generated files.
 */
public class ByteArrayResourceStream implements IResourceStream
{
	private static final long serialVersionUID = 1L;
	private Locale locale = null;
	private byte[] content = null;
	private String contentType = null;
	
	
	public ByteArrayResourceStream(byte[] content, String contentType)
	{
		this.content = content;
		this.contentType = contentType;
	}
	
	public void close() throws IOException
	{
		// nothing
	}
	
	public String getContentType()
	{
		return (contentType);
	}
	
	public InputStream getInputStream() throws ResourceStreamNotFoundException
	{
		return (new ByteArrayInputStream(content));
	}
	
	public Locale getLocale()
	{
		return (locale);
	}
	
	public Bytes length()
	{
		return null; // TODO : (content.length);
	}
	
	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}
	
	public Time lastModifiedTime()
	{
		return null;
	}

	public String getStyle() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getVariation() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setStyle(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setVariation(String arg0) {
		// TODO Auto-generated method stub
		
	}
}
