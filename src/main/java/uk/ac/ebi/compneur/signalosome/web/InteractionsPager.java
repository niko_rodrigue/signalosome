package uk.ac.ebi.compneur.signalosome.web;


import java.util.ArrayList;
import java.util.List;

import net.databinder.models.hib.CriteriaFilterAndSort;
import net.databinder.models.hib.HibernateListModel;
import net.databinder.models.hib.HibernateProvider;

import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilteredAbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.GoAndClearFilter;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.TextFilteredPropertyColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.AnnotatedLinkPanel;


/**
 * Displays paginated, filtered, and sorted Interaction objects.
 */
public class InteractionsPager extends BasePage
{
	// Cached list of Interaction ??
	HibernateListModel<Interaction> interactions = new HibernateListModel<Interaction>("from Interaction order by name");

	public InteractionsPager()
	{
		super();
		
		// list of columns allows table to render itself without any markup from us
		@SuppressWarnings("unchecked")
		List<IColumn<Interaction>> columns = new ArrayList<IColumn<Interaction>>();
		
		columns.add(new FilteredAbstractColumn<Interaction>(new Model<String>("Actions")) { // use "" for no column header
                public void populateItem(Item cellItem, String interactionId, IModel rowModel)
                {
                    cellItem.add(new AnnotatedLinkPanel(interactionId, rowModel));
                }
				// return the go-and-clear filter for the filter toolbar
				public org.apache.wicket.Component getFilter(String interactionId, FilterForm form)
				{
					return new GoAndClearFilter(interactionId, form);
				}
            });
		columns.add(new TextFilteredPropertyColumn<Interaction, String>(new Model<String>("Id"), "id", "id"));
		columns.add(new TextFilteredPropertyColumn<Interaction, String>(new Model<String>("Name"), "name", "name"));
		columns.add(new TextFilteredPropertyColumn<Interaction, String>(new Model<String>("UserId"), "userId", "userId"));
		
		CriteriaFilterAndSort builder = new CriteriaFilterAndSort(new uk.ac.ebi.compneur.signalosome.data.Interaction(), "name", true, false);
		FilterForm form = new FilterForm("form", builder);
		HibernateProvider<Interaction> provider = new HibernateProvider<Interaction>(uk.ac.ebi.compneur.signalosome.data.Interaction.class, builder);
		provider.setWrapWithPropertyModel(false);

		DataTable<Interaction> table = new DataTable<Interaction>("interactions", columns, provider, 50) {
			@Override protected Item<Interaction> newRowItem(String id, int index, IModel<Interaction> model)
			{
				return new OddEvenItem<Interaction>(id, index, model);
			}
		};
		table.addTopToolbar(new AjaxNavigationToolbar(table));
		table.addTopToolbar(new FilterToolbar(table, form, builder));
		table.addTopToolbar(new AjaxFallbackHeadersToolbar(table, builder));
		table.addBottomToolbar(new AjaxNavigationToolbar(table));
		add(form.add(table));
	}
}
