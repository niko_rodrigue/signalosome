package uk.ac.ebi.compneur.signalosome.web;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;

public class DeleteAnnotationPage extends BasePage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public DeleteAnnotationPage(Annotation annotation) 
    {
    	super(new Model<Annotation>(annotation));

    	init();
    }
	
    public DeleteAnnotationPage(PageParameters parameters) 
    {
    	super();
    	
    	String id = parameters.get("id").toString();
    	String annotation_id = parameters.get("annotation_id").toString();
    	Class<? extends Annotated> annotatedClass = null;
    	
    	if (id == null || id.trim().length() == 0) {
    		getSession().info("Cancelled delete, the given id was empty or missing !!");
    		throw new RestartResponseException(FrontPage.class, new PageParameters());
    	} 
    	else if (id.startsWith("INTR")) 
    	{
    		annotatedClass = Interaction.class;
    	}
    	else if (id.startsWith("COMP")) 
    	{
    		annotatedClass = Component.class;
    	}
    	else if (id.startsWith("PRMT")) 
    	{
    		annotatedClass = Parameter.class;
    	}
    	else 
    	{
    		getSession().info("Cancelled delete, the id '" + id + "' was not recognized !!");

    		throw new RestartResponseException(FrontPage.class, new PageParameters());
    	}

    	
    	HibernateObjectModel<Annotated> model = new HibernateObjectModel<Annotated>(annotatedClass, id);
    	
    	for (Annotation anno : model.getObject().getAnnotations()) 
    	{    		
    		if (anno.getId().equals(annotation_id)) 
    		{
    	    	setDefaultModel(new Model<Annotation>(anno));
    	    	break;
    		}
    	}
    	
    	init();
    }

	public DeleteAnnotationPage(final Page backPage, IModel<Annotation> model)
	{
		super(model);
		init();
	}
	
	private void init() 
	{
		Annotation annotation = (Annotation) getDefaultModelObject();
		
		add(new Label("name", String.format(" annotation %s on %s",annotation.getUrn(),annotation.getExternalId()) ) );

		DataForm<Annotation> form = new DataForm<Annotation>("confirmForm", new HibernateObjectModel<Annotation>(annotation))
		{
			private static final long serialVersionUID = 1L;

			protected void onSubmit()
			{
				Annotation annotated = getModelObject();
				Session session = Databinder.getHibernateSession();
				session.delete(annotated);
				session.flush();
				session.getTransaction().commit();
				getSession().info(String.format("Deleted %s %s on %s,",annotated.getId(),annotated.getUrn(), annotated.getExternalId()));
				setResponsePage(new FrontPage());
			}
		};

		form.add(new Button("cancel")
		{
			private static final long serialVersionUID = 1L;

			public void onSubmit()
			{
				setResponsePage(new FrontPage());
			}
		}.setDefaultFormProcessing(false));

		add(form);

	}
}