package uk.ac.ebi.compneur.signalosome.web;

import java.util.List;

import net.databinder.models.hib.CriteriaBuilder;
import net.databinder.models.hib.HibernateListModel;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListChoice;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.hibernate.Criteria;

import uk.ac.ebi.compneur.signalosome.data.Comment;
import uk.ac.ebi.compneur.signalosome.data.Component;


/**
 * Testing page (seems not XHTML valid so far).
 */
public class Testing extends BasePage
{
    /**
     * Constructor
     */
    public Testing()
    {

        add(new Label("subtitle", "Testing"));

        // list all Component
        HibernateListModel<Component> componentListModel = new HibernateListModel<Component>(Component.class);
        
        // Specific criteria
        CriteriaBuilder criteriaB = new CriteriaBuilder()
        {
            public void build(Criteria arg0)
            {
                // TODO Auto-generated method stub
            }
        };
        
        // HibernateObjectModel<Component> componentObjModel = new HibernateObjectModel<Component>(new QueryBinderBuilder("from component where id = 'COMP_000000006'"));
        HibernateListModel<Component> componentIdQuery1 = new HibernateListModel<Component>("from Component where id = 'COMP_000000006'");
        HibernateListModel<Component> componentIdQuery = new HibernateListModel<Component>("from Component comp where comp.name between 'A' and 'D' order by name");
        
        List<uk.ac.ebi.compneur.signalosome.data.Component> queriedComponent = componentIdQuery1.getObject();
        
        // add(new DataPanel<Component>("componentTable", componentObjModel));
        
        Form form = new Form("form") {
            protected void onSubmit() {
                info("the form was submitted!");
            }
        };
        
        // Add a form with an onSubmit implementation that sets a message
        add(form);
        
        ListChoice<Component> listChoice = new ListChoice<Component>("components", componentIdQuery);
        listChoice.setMaxRows(8);
        form.add(listChoice);
        
        
        ListChoice<Component> listChoice2 = new ListChoice<Component>("annotations", queriedComponent);
        listChoice2.setMaxRows(8);
        form.add(listChoice2);
        
        
        
        add(new ListView<Component>("componentList", componentIdQuery) {

            @Override
            protected void populateItem(ListItem<Component> item) {
                Component component = item.getModelObject();
                item.add(new Label("id", component.getId()));
                item.add(new Label("name", component.getName()));
                
                List<Comment> comments = component.getComments();
                if (comments != null && comments.size() > 0) {
                    item.add(new Label("comment", comments.get(0).getComment()));
                } else {
                    item.add(new Label("comment", ""));
                }
            }
            
        });
        
        
        // List<Component> components = componentListModel.getObject();
        
        if (queriedComponent.size() > 0) {
            Component component = queriedComponent.get(0);
            System.out.println("Queried Component = " + component.getId() + ", " + component.getName());
        	
        }
        if (componentIdQuery.getObject().size() > 0) {
            Component component = componentIdQuery.getObject().get(0);
            System.out.println("Queried Component = " + component.getId() + ", " + component.getName());
        	
        }
        
        // ((DataApplication) getApplication()).getHibernateSessionFactory(null).getCurrentSession().disconnect();
    }
}
