package uk.ac.ebi.compneur.signalosome.util;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class used only to load the annotated object linked to a parameter.
 * 
 * @author rodrigue
 *
 */
@Entity
@Table(name="param_rel")
public class ParameterRelation {

	@Id
	private String parameter_id;
	
	private String external_id;
	private String external_type;
	
	// empty constructor
	public ParameterRelation() {
		
	}
	
	public void setParameter_id(String parameter_id) {
		this.parameter_id = parameter_id;
	}
	public String getParameter_id() {
		return parameter_id;
	}
	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}
	public String getExternal_id() {
		return external_id;
	}
	public void setExternal_type(String external_type) {
		this.external_type = external_type;
	}
	public String getExternal_type() {
		return external_type;
	}
	
	
}
