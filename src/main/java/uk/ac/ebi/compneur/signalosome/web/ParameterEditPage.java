package uk.ac.ebi.compneur.signalosome.web;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateListModel;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.Strings;
import org.apache.wicket.validation.validator.StringValidator;
import org.hibernate.Query;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.Unit;
import uk.ac.ebi.compneur.signalosome.util.ParameterRelation;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.SBOTextField;
import uk.ac.ebi.compneur.signalosome.web.wicket.model.SBOAnnotationModel;
import uk.ac.ebi.compneur.signalosome.web.wicket.model.StringModel;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.DoubleValidator;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.ParameterNameValidator;


public class ParameterEditPage extends AnnotatedEditPage
{
	
	private List<String> valueTypes = Arrays.asList(new String[]{"min/max", "mean/std"});
	private SBOTextField sboTextField;
	private AutoCompleteTextField<Annotated> componentInteractionTextField; 
	private DropDownChoice<String> unitDropdown;
	private List<Annotated> componentAndInteractions;
	private RadioChoice<String> valueTypeChoice;
	private TextField<String> value1TextField;
	private TextField<String> value2TextField;
	private Label value1Label;
	private Label value2Label;
	private Parameter parameter;
	private List<Unit> units;
	private List<String> usedUnits;
	
	private String annotatedId = null;

    private boolean isNew = false;
    private ParameterNameValidator parameterNameValidator = new ParameterNameValidator();
    
	/**
	 * Creates a new ParameterEditPage with the given parameters
	 * 
	 * @param parameters
	 */
	public ParameterEditPage(PageParameters parameters)
	{
		this(new ParametersPager(), new HibernateObjectModel<Parameter>(Parameter.class, parameters.get("id") + ""), parameters.get("action").toString());
	}
	
	public ParameterEditPage(final Page backPage, final HibernateObjectModel<Parameter> model, String action)
	{
		super(backPage, model);
		
		parameter = model.getObject();
		
		if (action == null) {
        	action = "view";
        }
        else if (action.equals("delete")) {
        	setResponsePage(new DeletePage(new PageParameters().add("id", parameter.getId())));
        }
		
		// title
		Label title = new Label("title", new Model<String>() {
            @Override
            public String getObject() {
                return isNew ?
                "New Parameter" : model.getObject().getId();
            }
        });
		add(title);
		parameterNameValidator.setOldName(parameter.getName());
		
		final DataForm<Parameter> form = new DataForm<Parameter>("parameterEditForm", model)
		{
			protected void onBeforeRender() {
				super.onBeforeRender();

				// set all the values in the form not associated directly with the parameter model
				
				parameter = getModelObject();
				
				// set component/interaction field
				
				Session session = Databinder.getHibernateSession();
				Query q = session.createQuery("from param_rel in class ParameterRelation where parameter_id =:id");				
				q.setParameter("id", parameter.getId());
				@SuppressWarnings("unchecked")
				List<ParameterRelation> parameterRelations = q.list();
								
				if (parameterRelations.size() > 0) {

					annotatedId = parameterRelations.get(0).getExternal_id();
					// logger.debug("ParameterEditPage : onBeforeRender : annotated id = " + annotatedId);
					
				} else {
					// logger.debug("ParameterEditPage : onBeforeRender : NOT able to find an annotated id !!!!!!");
				}
				
				if (parameterRelations.size() > 1) {
					// logger.debug("ParameterEditPage : onBeforeRender : There is more than one parameter relations for parameter.getId() !!!!!!!");
				}
				
				Annotated annotated = null;
				if (annotatedId != null && annotatedId.startsWith("COMP_")) {
					Component component = (Component) session.get(Component.class, annotatedId);
					annotated = component;
				} else if (annotatedId != null && annotatedId.startsWith("INTR_")) {
					Interaction interaction = (Interaction) session.get(Interaction.class, annotatedId);
					annotated = interaction;
				}
				
				if (annotated != null) {
					componentInteractionTextField.setModelObject(annotated);
				}
				
				// TODO : how to know which numbers to get !!!
				if (parameter.getValueMean() != 0) {
					
					// logger.debug("ParameterEditPage : onBeforeRender : mean/std");
	
					valueTypeChoice.setModelObject("mean/std");
					value1TextField.setModelObject(parameter.getValueMean() + "");
					value2TextField.setModelObject(parameter.getStd() + "");

				} else {
					
					// logger.debug("ParameterEditPage : onBeforeRender : min/max");
					
					value1Label.setDefaultModelObject("Min:");
					value2Label.setDefaultModelObject("Max:");
					valueTypeChoice.setModelObject("min/max");
					value1TextField.setModelObject(parameter.getValueMin() + "");
					value2TextField.setModelObject(parameter.getValueMax() + "");
				}
				
				// set the unit
				int i = 0;
				int unitIndex = -1;
				for (Unit unit : units) {
					if (unit.getTags().contains("signalosome")) {
						String siUnit = unit.getExpressionBaseSIUnit();
						
						if (siUnit != null && siUnit.equals(parameter.getUnit())) {
							unitIndex = i;
							break;
						}
						
						String unitSymbol = unit.getSymbol();
						
						if (unitSymbol.equals(parameter.getUnit())) {
							unitIndex = i;
							break;
						}
					}
					i++;
				}

				// logger.debug("ParameterEditPage : onBeforeRender : units sizes = " + units.size() + ", " + usedUnits.size());
				
				if (unitIndex != -1) {
					System.out.println("ParameterEditPage : onBeforeRender : unitIndex = " + unitIndex + ", unit name = " + units.get(unitIndex).getName());
					unitDropdown.setModelObject(usedUnits.get(unitIndex));
				}
			}
			
			protected void onSubmit()
			{
				isNew = false;
				// TODO : check if we can get rid of all/most of the session.save methods call
				// and replace them with a single session.getTransaction().commit() call
				
				parameter = getModelObject();
				// TODO : HACK until authentication works again parameter.setUserId(getLoggedUser().getUsername());
				parameter.setUserId("martinaf");
				parameterNameValidator.setOldName(parameter.getName());
				
				Session session = Databinder.getHibernateSession();
				
				// Updating the SBO term : everything should be done in the setObject of the SBOAnnotationModel
				// We just need to save the new annotation object in fact !!
				Annotation sboAnnotation = sboTextField.getSBOAnnotation();
				if (sboAnnotation != null)
				{
					// TODO : this is adding some history even if the SBO annotation did not changed
					// we need to add a isDirty method to the SBOTextField
					session.saveOrUpdate(sboAnnotation);
				}

				// set the unit
				// TODO : make sure that usedUnits and Units contain the same units, could be different if
				// some unit had no 'signalosome' tag in the database.
				int unitIndex = -1;
				try {
					String unitIndexStr = unitDropdown.getValue();
					unitIndex = Integer.valueOf(unitIndexStr);
				} catch (NumberFormatException e) {
					// does nothing					
				} catch (ClassCastException e) {
					// does nothing
				}
				
				if (unitIndex != -1) {
					parameter.setUnit(units.get(unitIndex).getExpressionBaseSIUnit());
				} else {
					parameter.setUnit("");
				}
				
				// get the component or interaction and add the parameter to it
				String annotatedName = componentInteractionTextField.getValue();
				
				System.out.println("ParameterEditPage : onSubmit : annotatedName =  " + annotatedName);
				
				Annotated annotated = null; 
					
				for (Annotated ann : componentAndInteractions) {
					if (ann.getName().equals(annotatedName)) {
						annotated = ann;
					}
				}
				
				if (annotated == null) {
					System.out.println("ParameterEditPage : onSubmit : no parameter association seems to have been done !!!!");

					// TODO : check if there was one before ??
					
				} else if (annotated.getId().equals(annotatedId)) {
					System.out.println("ParameterEditPage : onSubmit : parameter association did not change.");
				} else {
					System.out.println("ParameterEditPage : onSubmit : parameter association did change !!!!");
					
					if (annotatedId != null) {
						// removing the old association
						if (annotatedId.startsWith("COMP_")) {
							Component component = (Component) session.get(Component.class, annotatedId);
							Parameter paramToRemove = null;
							
							for (Parameter param : component.getParameters()) {
								if (param.getId().equals(parameter.getId())) {
									paramToRemove = param;
								}
							}
							if (paramToRemove != null) {
								component.getParameters().remove(paramToRemove);
							}
							
						} else if (annotatedId != null && annotatedId.startsWith("INTR_")) {
							Interaction interaction = (Interaction) session.get(Interaction.class, annotatedId);
							Parameter paramToRemove = null;
							
							for (Parameter param : interaction.getParameters()) {
								if (param.getId().equals(parameter.getId())) {
									paramToRemove = param;
								}
							}
							if (paramToRemove != null) {
								interaction.getParameters().remove(paramToRemove);
							}
						}
					}
					
					// We cannot used the old annotated object that was on the list
					if (annotated instanceof Component) {
						Component component = (Component) session.get(Component.class, annotated.getId());
						component.addParameter(parameter);
						session.saveOrUpdate(component);
					} else if (annotated instanceof Interaction) {
						Interaction interaction = (Interaction) session.get(Interaction.class, annotated.getId());
						interaction.addParameter(parameter);
						session.saveOrUpdate(interaction);
					}					
				}
				
				if (valueTypeChoice.getModelObject().equals("mean/std"))
				{
					parameter.setValueMean(Double.parseDouble(value1TextField.getValue()));
					parameter.setStd(Double.parseDouble(value2TextField.getValue()));
				}
				else
				{
					parameter.setValueMin(Double.parseDouble(value1TextField.getValue()));
					parameter.setValueMax(Double.parseDouble(value2TextField.getValue()));
				}
				
				
				// TODO : this is adding some history even if the SBO annotation did not changed
				session.saveOrUpdate(parameter);
				setPersistentObject(parameter);
				session.flush();
				session.getTransaction().commit();
				session.disconnect();
				getSession().info(String.format("Saved Parameter %s %s",parameter.getId(),parameter.getName()));
				setResponsePage(backPage);
			}
		};
		add(form);
		
		form.add(new Button("new")
		{
			public void onSubmit()
			{
                isNew = true;
				Parameter parameter = new Parameter(); 
				parameter.setName("name");
				// TODO : HACK until authentication works again parameter.setUserId(getLoggedUser().getUsername());
				parameter.setUserId("martinaf");

				// reset and replace the object store in the model of this page
				setAnnotatedObject(parameter, form);
				
				componentInteractionTextField.setModel(new Model<Annotated>(null));
				componentInteractionTextField.clearInput();

				unitDropdown.setModelObject(null);

				sboTextField.setModel(new SBOAnnotationModel(parameter));

				setParameterUIEnabled(true);
				// disable the component and annotation interface
				setMetadataEnabled(false);
			}
		}.setDefaultFormProcessing(false));

        form.add(new Button("edit") {

            public void onSubmit() {

                // enable the component and annotation interface
                setMetadataEnabled(true);

                setParameterUIEnabled(true);
            }

        }.setDefaultFormProcessing(false));

        
        form.add(new Button("cancel") {
            public void onSubmit() {
                getSession().info("Cancelled edit");
                
                setResponsePage(backPage);
            }
        }.setDefaultFormProcessing(false));

        form.add(new Button("delete") {

            @Override
            public void onSubmit() {

                Annotated annotated = model.getObject();
                
                throw new RestartResponseException(DeletePage.class, new PageParameters().add("id", annotated.getId()));
            }
        });

		// name
		form.add(new RequiredTextField<String>("name").add(StringValidator.maximumLength(232)).add(parameterNameValidator));
		
		// custom field as it is an annotation to get 
		sboTextField = new SBOTextField(model.getObject()); 
		form.add(sboTextField);
		
		// list of reactions and component
		
		// Getting a list of all components
		HibernateListModel<Component> componentListModel = new HibernateListModel<Component>(Component.class);
		List<Component> components = componentListModel.getObject();
		// Getting a list of all interactions
		HibernateListModel<Interaction> reactionListModel = new HibernateListModel<Interaction>(Interaction.class);
		List<Interaction> interactions = reactionListModel.getObject();
		
		componentAndInteractions = new ArrayList<Annotated>();
		componentAndInteractions.addAll(components);
		componentAndInteractions.addAll(interactions);

		componentInteractionTextField = new AutoCompleteTextField<Annotated>("compIntFullList", new Model<Annotated>(null))
		{
			@Override
			protected Iterator<Annotated> getChoices(String input)
			{
				input = input.trim();
				
				if (Strings.isEmpty(input))
				{
					return componentAndInteractions.iterator();
				}
				
				List<Annotated> choices = new ArrayList<Annotated>(10);
				
				for (final Annotated annotated: componentAndInteractions)
				{
					if (annotated.getName().toUpperCase().contains(input.toUpperCase()))
					{
						choices.add(annotated);
						if (choices.size() == 20)
						{
							break;
						}
					}
				}
				
				return choices.iterator();
			}
			
			
		};
		form.add(componentInteractionTextField);

		valueTypeChoice = new RadioChoice<String>("valueType", new Model<String>("mean/std"), valueTypes) {
			public void onSelectionChanged()
			{
				if (valueTypeChoice.getValue().equals("1"))
				{
					value1Label.setDefaultModelObject("Mean:");
					value2Label.setDefaultModelObject("Std:");
				}
				else
				{
					value1Label.setDefaultModelObject("Min:");
					value2Label.setDefaultModelObject("Max:");
				}
			}
			
			@Override
			protected void onSelectionChanged(Object newSelection)
			{
				System.out.println("valueTypeChoice : onSelectionChanged(Object) called with" + newSelection);
			}
			
			@Override
			protected boolean wantOnSelectionChangedNotifications()
			{
				return true;
			}
		}.setSuffix(" "); // the setSuffix is to make the radio choice horizontal.
		form.add(valueTypeChoice);
		
		// Values
		
		value1Label = new Label("value1Label", new Model<String>("Mean:"));
		form.add(value1Label);
		value2Label = new Label("value2Label", new Model<String>("Std:"));
		form.add(value2Label);
		
		// first value field
		value1TextField = new TextField<String>("value1", new StringModel(""));
		value1TextField.add(new DoubleValidator());
		form.add(value1TextField);

		// second value field
		value2TextField = new TextField<String>("value2", new Model<String>(""));
		value2TextField.add(new DoubleValidator());
		form.add(value2TextField);

		// Getting a list of all units
		HibernateListModel<Unit> unitListModel = new HibernateListModel<Unit>(Unit.class);
		units = unitListModel.getObject();
		usedUnits = new ArrayList<String>();
		
		for (Unit unit : units) {
			if (unit.getTags().contains("signalosome")) {
				String displayUnit = unit.getExpressionBaseSIUnit();
				
				if (displayUnit == null || displayUnit.length() == 0) {
					displayUnit = unit.getSymbol();
				}
				
				if (unit.getName() != null && unit.getName().length() > 0) {
					displayUnit += " (" + unit.getName() + ")";
				}
				
				usedUnits.add(displayUnit);
			}
		}

		// list of units
		unitDropdown = new DropDownChoice<String>("unitDropdown", new Model<String>(""), usedUnits);
		form.add(unitDropdown);
		
        if (action.equals("view")) 
        { 
            // disable the component and annotation interface
            setMetadataEnabled(false);

            // disable the parameter interface
            setParameterUIEnabled(false);
        }

	}

	protected void setParameterUIEnabled(boolean enabled) 
	{
		sboTextField.setEnabled(enabled);
		componentInteractionTextField.setEnabled(enabled);
		value1TextField.setEnabled(enabled);
		value2TextField.setEnabled(enabled);
		valueTypeChoice.setEnabled(enabled);
		unitDropdown.setEnabled(enabled);
	}
}
