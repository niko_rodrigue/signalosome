package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;


/**
 * Identifier generator for Hibernate.
 * <p>Allow the generation and incrementation of identifiers like "COMP_000000001".
 * 
 * @author Camille Laibe
 * @version 20100616
 */
public class AnnotatedIdGenerator implements IdentifierGenerator
{
    /**
     * Default constructor (builds an empty object).
     */
    public AnnotatedIdGenerator()
    {
        // nothing here.
    }
    
    
    /**
     * Generates a new identifier for a <code>Annotated</code> objects (these could be Component, Interaction or Parameter).
     * 
     * @throws IdentifierOverflowException
     * @see org.hibernate.id.IdentifierGenerator#generate(org.hibernate.engine.SessionImplementor, java.lang.Object)
     */
    public Serializable generate(SessionImplementor session, Object object) throws HibernateException
    {
        String identifier = null;
        
        if (null != object)
        {
            String lastId = null;
            
            // retrieves the name of the table where the object is stored
            Table table = object.getClass().getAnnotation(Table.class);
            String tableName = table.name();
            
            // query to retrieve the highest identifier
            String query = "SELECT MAX(id) FROM " + tableName;
            
            try
            {
                PreparedStatement st = session.connection().prepareStatement(query);
                
                try
                {
                    ResultSet rs = st.executeQuery();
                    try
                    {
                        rs.first();
                        lastId = rs.getString(1);
                        
                        //System.out.println("Id fetched: " + lastId);   // DEBUG
                        
                        // retrieves the last part of the identifier (composed of digits)
                        if (lastId == null) {
                        	// It is the first row on the table, so we have to create the first id
                        	
                        	if (object instanceof Component)
                        	{
                        		lastId = "COMP_000000000";
                        	}
                        	else if (object instanceof Interaction) 
                        	{
                        		lastId = "INTR_000000000";
                        	}
                        	else if (object instanceof Parameter) 
                        	{
                        		lastId = "PRMT_000000000";
                        	}
                        	else if (object instanceof Annotation) 
                        	{
                        		lastId = "ANNO_000000000";
                        	}
                        	else if (object instanceof Participant) 
                        	{
                        		lastId = "PRTP_000000000";
                        	}
                        	else if (object instanceof Tag) 
                        	{
                        		lastId = "TAGG_000000000";
                        	}
                        }
                        
                        String[] idSplit = lastId.split("_");
                        String prefix = idSplit[0];
                        int suffix = Integer.parseInt(idSplit[1]);
                        
                        //System.out.println("Last identifier used in the database: " + prefix + "[_]" + suffix);   // DEBUG
                        
                        // generates the new identifier
                        String newId = prefix + "_" + String.format("%9d", ++suffix);
                        newId = newId.replace(" ", "0");
                        
                        //System.out.println("DataType identifier generated: " + newId);   // DEBUG
                        
                        // checks if there is no identifier overflow (the identifier is too long to follow the pattern: \w{4}_\d{9})
                        if (suffix > 999999999)
                        {
                            IdentifierOverflowException ex = new IdentifierOverflowException("Can't generate a new identifier! The last one is: " + lastId);
                            throw ex;
                        }
                        
                        identifier = newId;
                    }
                    finally
                    {
                        rs.close();
                    }
                }
                finally
                {
                    st.close();
                }
            }
            catch (SQLException sqle)
            {
                throw new HibernateException("Could not get next sequence value (" + query);
            }
        }
        
        return identifier;
    }


}
