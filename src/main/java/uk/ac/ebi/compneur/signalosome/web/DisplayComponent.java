package uk.ac.ebi.compneur.signalosome.web;


import net.databinder.models.hib.HibernateListModel;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import uk.ac.ebi.compneur.signalosome.data.Component;


public class DisplayComponent extends BasePage
{
    public DisplayComponent(PageParameters params)
    {
        // subtitle
        add(new Label("subtitle", "Component"));
        // links
        add(new BookmarkablePageLink<BasePage>("link_intro", FrontPage.class));
        
        // retrieve the proper component
        // TODO
        
        // test
        add(new Label("id", params.get("cid").toString()));
        HibernateListModel<Component> componentIdQuery = new HibernateListModel<Component>("from Component where id = '" + params.get("cid").toString() + "'");
        
        if (componentIdQuery.getObject().size() != 1) {
        	// TODO : Display an error message
        	
        } else {
        
        	Component component = componentIdQuery.getObject().get(0);
        	
        	add(new Label("name", component.getName()));
        	
        	// TODO : create a proper page with all the fields
        }
        
    }
}
