package uk.ac.ebi.compneur.signalosome.web.wicket.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.util.string.Strings;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.web.wicket.model.SBOAnnotationModel;
import uk.ac.ebi.ontology.sbo.SBO;

public class SBOTextField extends AutoCompleteTextField<String> {
	

	/**
	 * Creates a new SBOTextField instance. It will be associated with 'sboTerm' in HTML.
	 * 
	 * @param annotated
	 */
	public SBOTextField(Annotated annotated) {
		super("sboTerm");
		
		setModel(new SBOAnnotationModel(annotated));
		if (((SBOAnnotationModel) getModel()).getObject() != null) {
			setLabel(getModel());
		}
	}
	
	@Override
	protected Iterator<String> getChoices(String input) {

		Collection<String> sboTermNames = SBO.getTermNames();
		
		input = input.trim();
		
		if (Strings.isEmpty(input))
		{
			return sboTermNames.iterator();
		}

		List<String> choices = new ArrayList<String>(10);

		for (final String sboTermName : sboTermNames)
		{

			if (sboTermName.toUpperCase().contains(input.toUpperCase()))
			{
				choices.add(sboTermName);
				if (choices.size() == 20)
				{
					break;
				}
			}
		}

		return choices.iterator();
	}

	/**
	 * Returns the SBO annotation of the underlying annotated object if it has one, null otherwise.
	 * 
	 * @return the SBO annotation of the underlying annotated object if it has one, null otherwise.
	 */
	public Annotation getSBOAnnotation() {
		return ((SBOAnnotationModel) getModel()).getSBOAnnotation();
	}
	

}

