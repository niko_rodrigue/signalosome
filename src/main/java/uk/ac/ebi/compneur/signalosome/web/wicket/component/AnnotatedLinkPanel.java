package uk.ac.ebi.compneur.signalosome.web.wicket.component;


import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.web.ComponentEditPage;
import uk.ac.ebi.compneur.signalosome.web.DeletePage;
import uk.ac.ebi.compneur.signalosome.web.InteractionEditPage;
import uk.ac.ebi.compneur.signalosome.web.ParameterEditPage;


public class AnnotatedLinkPanel extends Panel
{
	private static final long serialVersionUID = -6333981925438844752L;
	
	public AnnotatedLinkPanel(String id, IModel<Annotated> model)
    {
        super(id, model);
        Annotated annotated = model.getObject();
        PageParameters parameters = new PageParameters();
        parameters.add("id", annotated.getId());
        
        if (annotated instanceof Component)
        {
        	parameters.add("action", "view");
        	add(new BookmarkablePageLink<ComponentEditPage>("view", ComponentEditPage.class, parameters));
        	add(new BookmarkablePageLink<ComponentEditPage>("edit", ComponentEditPage.class, new PageParameters().add("id", annotated.getId()).add("action", "edit")));
        	add(new BookmarkablePageLink<ComponentEditPage>("delete", DeletePage.class, new PageParameters().add("id", annotated.getId())));
        }
        else if (annotated instanceof Interaction)
        {
        	parameters.add("action", "view");
        	add(new BookmarkablePageLink<InteractionEditPage>("view", InteractionEditPage.class, parameters));
        	add(new BookmarkablePageLink<InteractionEditPage>("edit", InteractionEditPage.class, new PageParameters().add("id", annotated.getId()).add("action", "edit")));
        	add(new BookmarkablePageLink<DeletePage>("delete", DeletePage.class, new PageParameters().add("id", annotated.getId())));
        }
        else if (annotated instanceof Parameter)
        {
        	parameters.add("action", "view");
        	add(new BookmarkablePageLink<ParameterEditPage>("view", ParameterEditPage.class, parameters));
        	add(new BookmarkablePageLink<ParameterEditPage>("edit", ParameterEditPage.class, new PageParameters().add("id", annotated.getId()).add("action", "edit")));
        	add(new BookmarkablePageLink<ParameterEditPage>("delete", DeletePage.class, new PageParameters().add("id", annotated.getId())));
        } 
    }
}

