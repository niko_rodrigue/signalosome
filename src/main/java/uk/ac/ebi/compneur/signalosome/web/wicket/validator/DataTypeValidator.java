
package uk.ac.ebi.compneur.signalosome.web.wicket.validator;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import uk.ac.ebi.miriam.db.DataType;


/**
 * Validator for checking if a given Datatype is a either void, null or not.
 * 
 * In fact, we don't need to use this validator as if there is nothing defined in a field 
 * or nothing selected in a dropdown, the validator are not called.
 * We have to use the .setRequired(true) on the component to tell
 * wicket that it cannot be null.
 * 
 */
public class DataTypeValidator implements IValidator<DataType>
{

	/**
	 * Constructor .
	 * 
	 */
	public DataTypeValidator()
	{
	}


	/** {@inheritDoc} */
	public void validate(IValidatable<DataType> validatable)
	{
		DataType data = validatable.getValue();
		
		
		if (data != null){
			// System.out.println("DataTypeValidator : datatype validator " + data.getName());
			return;
		}
		else 
		{
			ValidationError error = new ValidationError();
			error.setMessage("You need to specify a valid datatype, no datatype was given");
			validatable.error(error);			
		}
	}
}