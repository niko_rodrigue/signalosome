package uk.ac.ebi.compneur.signalosome.web;


import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.util.HibernateUtil;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.AnnotatedListPanel;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.ComponentListPanel;


/**
 * Search Page
 */
public class Search extends BasePage
{
	String searchType;
	
    public Search(PageParameters param) {
    	super();
    	
    	searchType = param.get("type").toString();
    	
    	if (searchType == null || searchType.trim().length() == 0) {
    		searchType = "component";
    	}
    	
    	Form form = new Form("search") {
    		
    	    
    	    protected void onSubmit() {
    	        System. out. println("form was submitted! ") ;
    	        // form.getRequest();
    	        
    	        String searchQuery = getWebRequest().getRequestParameters().getParameterValue("searchQuery").toString();

    	        System. out. println("Search query =  " + searchQuery) ;
    	        
    	        List<? extends Annotated> matchedAnnotateds = null;
    	        
    	        if (searchType.equals("interaction")) 
    	        {
    	        	matchedAnnotateds = HibernateUtil.searchInteractions(searchQuery);
        	        @SuppressWarnings("unchecked")
    				AnnotatedListPanel componentListPanel = new AnnotatedListPanel("resultListPanel", "Found Interactions", (List<Annotated>) matchedAnnotateds);

                    getPage().replace(componentListPanel);
    	        }
    	        else 
    	        {
    	        	matchedAnnotateds = HibernateUtil.searchComponents(searchQuery);
        	        @SuppressWarnings("unchecked")
    				ComponentListPanel componentListPanel = new ComponentListPanel("resultListPanel", "Found Components", (List<Component>) matchedAnnotateds);

                    getPage().replace(componentListPanel);
    	        }
    	        
    	        for (Annotated annotated : matchedAnnotateds)
    	        {
    	        	System.out.println(annotated.getId());
    	        }
    	        
                // set the component lists

    	        
    	    }
    	};
    	
    	add(form) ;
    	
    	form. add(new TextField("searchQuery" , new Model("")).setRequired(true)) ;
    	
        List<Component> elements = new ArrayList<Component>();
        ComponentListPanel ComponentListPanel = new ComponentListPanel("resultListPanel", "Results", elements);
        ComponentListPanel.setVisible(elements != null && !elements.isEmpty());
        add(ComponentListPanel);
 
    }
}
