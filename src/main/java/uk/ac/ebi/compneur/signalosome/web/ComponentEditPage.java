package uk.ac.ebi.compneur.signalosome.web;

import java.util.ArrayList;
import java.util.List;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.ParameterListPanel;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.ComponentNameValidator;

public class ComponentEditPage extends AnnotatedEditPage {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ComponentNameValidator componentNameValidator = new ComponentNameValidator();

    public ComponentEditPage(PageParameters parameters) {
        this(new ComponentsPager(), new HibernateObjectModel<Component>(Component.class, parameters.get("id") + ""), parameters.get("action").toString());
    }

    public ComponentEditPage(final Page backPage, final HibernateObjectModel<Component> model, String action) 
    {
        super(backPage, model);

        if (action == null) {
        	action = "view";
        }
        else if (action.equals("delete")) {
        	setResponsePage(new DeletePage(new PageParameters().add("id", model.getObject().getId())));
        }
        

        // title
        Label title = new Label("title", new Model<String>() {
            @Override
            public String getObject() {
                return componentNameValidator.isNew() ?
                        "New Component" : model.getObject().getId();
            }
        });
        add(title);

        final DataForm<Component> form = new DataForm<Component>("editform1", model) {
            protected void onSubmit() {
                Component component = getModelObject();
                // TODO : HACK until authentication works again component.setUserId(getLoggedUser().getUsername());
                component.setUserId("martinaf");

                Session session = Databinder.getHibernateSession();
                session.saveOrUpdate(component);
                setPersistentObject(component);
                session.flush();
                session.getTransaction().commit();
                session.disconnect();
                getSession().info(String.format("Saved component %s %s", component.getId(), component.getName()));
                setResponsePage(backPage);
            }
        };
        add(form);
        componentNameValidator.setOldName(form.getModelObject().getName());

        form.add(new RequiredTextField<String>("name").add(StringValidator.lengthBetween(1, 32)).add(componentNameValidator));

        form.add(new Button("new") {
            public void onSubmit() {
                Component component = new Component();
                component.setName("name");
                // TODO : HACK until authentication works again component.setUserId(getLoggedUser().getUsername());
                component.setUserId("martinaf");
                componentNameValidator.setOldName(null);
                // reset and replace the object store in the model of this page
                setAnnotatedObject(component, form);

                // disable the component and annotation interface
                setMetadataEnabled(false);
                
                // reset the parameter lists
                ParameterListPanel parameterListPanel = new ParameterListPanel("parameterList", new ArrayList<Parameter>());
                parameterListPanel.setVisible(false);
                getPage().replace(parameterListPanel);
                
            }
        }.setDefaultFormProcessing(false));

        form.add(new Button("edit") {

            public void onSubmit() {

                // enable the component and annotation interface
                setMetadataEnabled(true);

                // enable the participant interface
                setParameterInterfaceEnabled(true);

            }

        }.setDefaultFormProcessing(false));

        
        form.add(new Button("cancel") {
            public void onSubmit() {
                getSession().info("Cancelled edit");
                
                setResponsePage(backPage);
            }
        }.setDefaultFormProcessing(false));

        form.add(new Button("delete") {

            @Override
            public void onSubmit() {

                Annotated annotated = model.getObject();
                
                throw new RestartResponseException(DeletePage.class, new PageParameters().add("id", annotated.getId()));
            }
        });

        
        List<Parameter> elements = model.getObject().getParameters();
        ParameterListPanel parameterListPanel = new ParameterListPanel("parameterList", elements);
        parameterListPanel.setVisible(elements != null && !elements.isEmpty());
        add(parameterListPanel);
        
        if (action.equals("view")) 
        { 
            // disable the component and annotation interface
            setMetadataEnabled(false);

            // disable the parameter interface
            setParameterInterfaceEnabled(false);
        }

    }

	private void setParameterInterfaceEnabled(boolean b) 
	{       
        // the list of parameters links
        getPage().get("parameterList").setEnabled(b); 
	}

}
