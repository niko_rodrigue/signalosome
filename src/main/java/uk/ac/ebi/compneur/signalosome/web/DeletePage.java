package uk.ac.ebi.compneur.signalosome.web;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotated;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;

public class DeletePage extends BasePage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
    public DeletePage(PageParameters parameters) 
    {
    	super();
    	
    	String id = parameters.get("id").toString();
    	Class<? extends Annotated> annotatedClass = null;
    	
    	if (id == null || id.trim().length() == 0) {
    		getSession().info("Cancelled delete, the given id was empty or missing !!");
    		throw new RestartResponseException(FrontPage.class, new PageParameters());
    	} 
    	else if (id.startsWith("INTR")) 
    	{
    		annotatedClass = Interaction.class;
    	}
    	else if (id.startsWith("COMP")) 
    	{
    		annotatedClass = Component.class;
    	}
    	else if (id.startsWith("PRMT")) 
    	{
    		annotatedClass = Parameter.class;
    	}
    	else 
    	{
    		getSession().info("Cancelled delete, the id '" + id + "' was not recognized !!");

    		throw new RestartResponseException(FrontPage.class, new PageParameters());
    	}

    	
    	HibernateObjectModel<Annotated> model = new HibernateObjectModel<Annotated>(annotatedClass, id);
    	
    	setDefaultModel(model);
    	init();
    }

	public DeletePage(final Page backPage, HibernateObjectModel<Annotated> model)
	{
		super(model);
		init();
	}
	
	private void init() {
		Annotated annotated = (Annotated) getDefaultModelObject();
		
		add(new Label("name", String.format("%s %s",annotated.getId(),annotated.getName()) ) );

		DataForm<Annotated> form = new DataForm<Annotated>("confirmForm", new HibernateObjectModel<Annotated>(annotated))
		{
			private static final long serialVersionUID = 1L;

			protected void onSubmit()
			{
				Annotated annotated = (Annotated) getModelObject();
				Session session = Databinder.getHibernateSession();
				session.delete(annotated);
				session.flush();
				session.getTransaction().commit();
				getSession().info(String.format("Deleted %s %s,",annotated.getId(),annotated.getName()));
				setResponsePage(new FrontPage());
			}
		};

		form.add(new Button("cancel")
		{
			private static final long serialVersionUID = 1L;

			public void onSubmit()
			{
				setResponsePage(new FrontPage());
			}
		}.setDefaultFormProcessing(false));

		add(form);

	}
}