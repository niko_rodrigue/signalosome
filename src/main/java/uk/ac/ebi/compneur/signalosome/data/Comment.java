package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;


/**
 * <p>Comment.
 * 
 * @author Camille Laibe <camille.laibe@ebi.ac.uk>
 */
@Entity
@Table(name="comment")
@Audited
public class Comment implements Serializable
{
    @Transient
    private static final long serialVersionUID = -400856609685005163L;
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private String id;
    @Column(name="comment")
    private String comment;
    @Column(name="user_id")
    private String user;
    @Column(name="date")
    private Date date;
    @Column(name="external_id")
    private String externalId;
    @Column(name="external_type")
    private String externalType;
    
    
    /**
     * Getter
     * @return the id
     */
    public String getId()
    {
        return this.id;
    }
    
    /**
     * Setter
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * Getter
     * @return the comment
     */
    public String getComment()
    {
        return this.comment;
    }
    
    /**
     * Setter
     * @param comment the comment to set
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
    /**
     * Getter
     * @return the user
     */
    public String getUser()
    {
        return this.user;
    }
    
    /**
     * Setter
     * @param user the user to set
     */
    public void setUser(String user)
    {
        this.user = user;
    }
    
    /**
     * Getter
     * @return the date
     */
    public Date getDate()
    {
        return this.date;
    }
    
    /**
     * Setter
     * @param date the date to set
     */
    public void setDate(Date date)
    {
        this.date = date;
    }


	public void setExternalId(String externalId)
	{
		this.externalId = externalId;
	}

	public String getExternalId()
	{
		return externalId;
	}

	public void setExternalType(String externalType)
	{
		this.externalType = externalType;
	}

	public String getExternalType()
	{
		return externalType;
	}

 
   /**
     * Getter
     * @return the date as a string
     */
    public String getDateString()
    {
      if (this.date != null)
      {
        return (this.date).toString();
      }
      else
      {
        return "XXXXXXX";
      }
    }
    
    /**
     * Returned a nicely formatted comment for display in HTML pages.
     * Conversions done:
     * - "\n" are converted into "<br />"
     * 
     * WARNING: not used as Wicket escapes the characters, use MultiLineLabel instead.
     * 
     * @param comment
     * @return
     */
    public static String getHtmlComment(String comment)
    {
    	return comment.replaceAll("\n", "<br />");
    }
}
