package uk.ac.ebi.compneur.signalosome.web;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.databinder.components.hib.DataForm;
import net.databinder.hib.Databinder;
import net.databinder.models.hib.HibernateListModel;
import net.databinder.models.hib.HibernateObjectModel;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.Strings;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;
import org.apache.wicket.validation.validator.StringValidator;
import org.hibernate.Session;

import uk.ac.ebi.compneur.signalosome.data.Annotation;
import uk.ac.ebi.compneur.signalosome.data.Component;
import uk.ac.ebi.compneur.signalosome.data.Interaction;
import uk.ac.ebi.compneur.signalosome.data.Parameter;
import uk.ac.ebi.compneur.signalosome.data.Participant;
import uk.ac.ebi.compneur.signalosome.util.HibernateUtil;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.ParameterListPanel;
import uk.ac.ebi.compneur.signalosome.web.wicket.component.SBOTextField;
import uk.ac.ebi.compneur.signalosome.web.wicket.model.SBOAnnotationModel;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.IntegerValidator;
import uk.ac.ebi.compneur.signalosome.web.wicket.validator.InteractionNameValidator;
import uk.ac.ebi.ontology.container.SignalosomeContainerOntology;


public class InteractionEditPage extends AnnotatedEditPage {

    // @Transient private transient Logger logger = Logger.getLogger(this.getClass());

    private Collection<String> containerNames = SignalosomeContainerOntology.getTermNames();
    
    private List<Component> components;

    private SBOTextField sboTextField;

    private Interaction interaction;
    private InteractionNameValidator interactionNameValidator = new InteractionNameValidator();


    public InteractionEditPage(PageParameters parameters) {
        this(new InteractionsPager(), new HibernateObjectModel<Interaction>(Interaction.class, parameters.get("id") + ""), parameters.get("action").toString());        
    }

    public InteractionEditPage(final Page backPage, final HibernateObjectModel<Interaction> model, String action) {
        super(backPage, model);

        interaction = model.getObject();

        // title
        Label title = new Label("title", new Model<String>() {
            @Override
            public String getObject() {
                return interactionNameValidator.isNew() ?
                "New Interaction" : model.getObject().getId();
            }
        });
        add(title);

        if (action.equals("delete")) {
        	setResponsePage(new DeletePage(new PageParameters().add("id", interaction.getId())));
        }
        
        // Getting a list of all components
        HibernateListModel<Component> componentListModel = new HibernateListModel<Component>(Component.class);
        components = componentListModel.getObject();

        final DataForm<Interaction> form = new DataForm<Interaction>("interactionEditform", model) {
            protected void onSubmit() {
                Interaction interaction = getModelObject();
                // TODO : HACK until authentication works again interaction.setUserId(getLoggedUser().getUsername());
                interaction.setUserId("martinaf");
                
                Session session = Databinder.getHibernateSession();

                // Updating the SBO term : everything should be done in the setObject of the SBOAnnotationModel
                // We just need to save the new annotation object in fact !!
                Annotation sboAnnotation = sboTextField.getSBOAnnotation();
                if (sboAnnotation != null) {
                    // TODO : this is adding some history even if the SBO annotation did not changed
                    // we need to add a isDirty method to the SBOTextField
                    session.saveOrUpdate(sboAnnotation);
                }

                session.saveOrUpdate(interaction);
                setPersistentObject(interaction);
                interactionNameValidator.setOldName(interaction.getName());
                session.flush();
                session.getTransaction().commit();
                session.disconnect();
                getSession().info(String.format("Saved Interaction %s %s", interaction.getId(), interaction.getName()));

                // TODO : enable the metadata interface and stay on the same page.
                setResponsePage(backPage);
                // setMetadataEnabled(true);
                // setParticipantInterfaceEnabled(true);
            }
        };
        add(form);

        // name
        form.add(new RequiredTextField<String>("name").add(StringValidator.lengthBetween(1, 150)).add(interactionNameValidator));

        // custom field as it is an annotation to get
        sboTextField = new SBOTextField(model.getObject());
        form.add(sboTextField);

        // TODO : create the participant component

        // TODO : check how to define the html id attribute that is automatically generated otherwise
        // TODO : check how to activate the auto-complete functionality.

        // TODO : when typing enter in the containers field, it is doing the same as clicking the new button !!

        // TODO : add a validator to check the value entered in the participant TextField

        // validation is done only when the submit button of the form is pressed, so we are making several forms.
        
        final DataForm<Interaction> participantForm = new DataForm<Interaction>("participantForm", model);

        final AutoCompleteTextField<Component> participantTextField =
                new AutoCompleteTextField<Component>("participant", new Model<Component>(null)) {

                    @Override
                    protected Iterator<Component> getChoices(String input) {
                        input = input.trim();

                        if (Strings.isEmpty(input)) {
                            return components.iterator();
                        }

                        List<Component> choices = new ArrayList<Component>(10);

                        for (final Component component : components) {

                            if (component.getName().toUpperCase().contains(input.toUpperCase())) {
                                choices.add(component);
                                if (choices.size() == 10) {
                                    break;
                                }
                            }
                        }

                        return choices.iterator();
                    }
                };
        participantForm.add(participantTextField);

        final AutoCompleteTextField<String> containerTextField = new AutoCompleteTextField<String>("containers", new Model<String>("")) {
            @Override
            protected Iterator<String> getChoices(String input) {
                input = input.trim();

                if (Strings.isEmpty(input)) {
                    return containerNames.iterator();
                }

                List<String> choices = new ArrayList<String>(10);

                for (final String containerName : containerNames) {

                    if (containerName.toUpperCase().contains(input.toUpperCase())) {
                        choices.add(containerName);
                        if (choices.size() == 10) {
                            break;
                        }
                    }
                }

                return choices.iterator();
            }
        };
        participantForm.add(containerTextField);

        // Stoichiometry is not directly in the interaction class
        final RequiredTextField<String> stoichiometryTextField = new RequiredTextField<String>("stoichiometry", new Model<String>("1"));
        stoichiometryTextField.add(new IntegerValidator());
        participantForm.add(stoichiometryTextField);

        // form.add(new SBOTextField(new Participant())); // Then in the add button, we have to get this or better make a Panel based on a Participant based model.

        containerTextField.add(new IValidator<String>() {

            public void validate(IValidatable<String> validatable) {
                String givenContainerName = validatable.getValue().trim();
                boolean nameIsValid = false;

                for (String containerName : containerNames) {
                    if (containerName.equals(givenContainerName)) {
                        nameIsValid = true;
                        break;
                    }
                }

                System.out.println("Container Validation : name, isValid = " + givenContainerName + ", " + nameIsValid);

                if (!nameIsValid) {
                    validatable.error(new ValidationError().setMessage("You have to give a valid container name from the signalosome container ontology."));
                }
            }

        });


        interactionNameValidator.setOldName(interaction.getName());

        // creates list of existing participants
        List<Participant> participants = interaction.getParticipants();

        // Hack to initialize everything for interactions as apparently the hibernate session is not valid anymore
        // inside exporter.writeAsSBMLModel(annotated);
        
        // TODO : we could resolve that by doing a specific form for the exportSBML button and having the default behavior set to true

        for (Participant participant : interaction.getParticipants()) {
            participant.getAnnotations().size();
            participant.getComments().size();
            participant.getComponent().getComments().size();
            participant.getComponent().getAnnotations().size();
        }
        // End of hack


        final ListView<Participant> listOfParticipants = new ListView<Participant>("participantList", participants) {
            @Override
            protected void populateItem(final ListItem<Participant> item) {
                final Participant c = item.getModelObject();
                Component component = c.getComponent();
                // item.add(new Label("id", component.getId()));
                item.add(new Label("participantName", component.getName()));

                Annotation locationAnno = c.getContainerAnnotation();
                String location = "";

                if (locationAnno != null) {
                    location = locationAnno.getMiriamId();
                }

                item.add(new Label("location", location));
                item.add(new Label("Stoichiometry", c.getStoichiometry() + ""));

                item.add(new Button("delParticipant") {

                    @Override
                    public void onSubmit() {
                        final Participant c = item.getModelObject();

                        Interaction interaction = participantForm.getModelObject();
                        Session session = Databinder.getHibernateSession();

                        System.out.println("\ndelParticipant : Before session flush\n");

                        Participant participantToDelete = (Participant) session.get(Participant.class, c.getId());


                        // Update the user on the annotation
                        Annotation containerAnnotation = participantToDelete.getContainerAnnotation();
                        if (containerAnnotation != null) {
                        	// TODO : HACK until authentication works again containerAnnotation.setUserId(getLoggedUser().getUsername());
                        	containerAnnotation.setUserId("martinaf");
                        }

                        interaction.getParticipants().remove(participantToDelete);

                        session.delete(participantToDelete);

                        session.saveOrUpdate(interaction);

                        System.out.println("\ndelParticipant : Before session flush\n");

                        session.flush();

                        // get the 'participantList' component
                        org.apache.wicket.Component participantListView = getPage().get("participantForm:participantList");

                        // reset the participant interface
                        participantListView.setDefaultModelObject(interaction.getParticipants());

                    }
                });


            }
        };
        // list directly added to the page
        participantForm.add(listOfParticipants);

        participantForm.add(new Button("addParticipant") {
            public void onSubmit() {
                Interaction interaction = participantForm.getModelObject();
                Participant participant = new Participant();

                Session session = Databinder.getHibernateSession();

                participant.setInteraction(interaction);
                participant.setStoichiometry(Integer.parseInt(stoichiometryTextField.getValue()));

                // get the component
                Component component = HibernateUtil.findComponentByName(participantTextField.getValue());

                participant.setComponent(component);

                System.out.println("Before saving participant");

                session.saveOrUpdate(participant);
                interaction.addParticipant(participant);

                // add the location/container annotation.
                Annotation locationAnno = new Annotation();
                locationAnno.setUrn(uk.ac.ebi.compneur.signalosome.data.Annotated.SIGNALOSOME_CONTAINER_ONTOLOGY_URN + ":" + containerTextField.getValue());
                locationAnno.setExternalId(participant.getId());
                locationAnno.setExternalType("participant");
                locationAnno.setQualifier("occursIn");
             // TODO : HACK until authentication works again locationAnno.setUserId(getLoggedUser().getUsername());
                locationAnno.setUserId("martinaf");

                // the checks that the component is not null, the location is valid and stoichiometry too are done via Validator beforehand

                System.out.println("Before saving participant location annotation");

                session.saveOrUpdate(locationAnno);
                participant.addAnnotation(locationAnno);

                System.out.println("\nBefore session flush\n");

                session.flush();

                // validate(); // to execute form validation
                // form.hasError(); // to find out whether validate() resulted in validation errors
                // ((DataForm<Interaction>) form).updateFormComponentModels(); // to update the models of nested form components.

                // get the 'participantList' component
                org.apache.wicket.Component participantListView = getPage().get("participantForm:participantList");

                // reset the participant interface
                participantListView.setDefaultModelObject(interaction.getParticipants());

                // TODO : reset the add participant field !
            }
        });

        add(participantForm);

        form.add(new Button("new") {

            public void onSubmit() {
                Interaction interaction = new Interaction();
                interaction.setName("name");
             // TODO : HACK until authentication works again interaction.setUserId(getLoggedUser().getUsername());
                interaction.setUserId("martinaf");
                interactionNameValidator.setOldName(null);
                // reset and replace the object store in the model of this page
                setAnnotatedObject(interaction, form);

                sboTextField.setModel(new SBOAnnotationModel(interaction));

                // get the 'participantList' component
                org.apache.wicket.Component participantListView = getPage().get("participantForm:participantList");
                // reset the participant interface
                participantListView.setDefaultModelObject(interaction.getParticipants());

                // reset the parameter lists
                ParameterListPanel parameterListPanel = new ParameterListPanel("parameterList", new ArrayList<Parameter>());
                parameterListPanel.setVisible(false);
                getPage().replace(parameterListPanel);
                
                // disable the component and annotation interface
                setMetadataEnabled(false);

                // disable the participant interface
                setParticipantInterfaceEnabled(false);

            }
        }.setDefaultFormProcessing(false));
        
        form.add(new Button("edit") {

            public void onSubmit() {

                // enable the component and annotation interface
                setMetadataEnabled(true);

                // enable the participant interface
                setParticipantInterfaceEnabled(true);

            }
        }.setDefaultFormProcessing(false));

        
        form.add(new Button("cancel") {
            public void onSubmit() {
                getSession().info("Cancelled edit");
                
                setResponsePage(backPage);
            }
        }.setDefaultFormProcessing(false));

        form.add(new Button("delete") {

            @Override
            public void onSubmit() {

                Interaction interaction = model.getObject();
                
                throw new RestartResponseException(DeletePage.class, new PageParameters().add("id", interaction.getId()));
            }
        });
        
        List<Parameter> elements = interaction.getParameters();
        ParameterListPanel parameterListPanel = new ParameterListPanel("parameterList", elements);
        parameterListPanel.setVisible(elements != null && !elements.isEmpty());
        add(parameterListPanel);
        
        if (action.equals("view")) 
        { 
            // disable the component and annotation interface
            setMetadataEnabled(false);

            // disable the participant interface
            setParticipantInterfaceEnabled(false);
        }
    }

    /**
     * Enables or disable the add participant components
     *
     * @param enabled
     */
    @SuppressWarnings("unchecked")
    private void setParticipantInterfaceEnabled(boolean enabled) {


        TextField<String> participantId = (TextField<String>) getPage().get("participantForm:participant");
        participantId.setEnabled(enabled);

        TextField<String> containerId = (TextField<String>) getPage().get("participantForm:containers");
        containerId.setEnabled(enabled);

        TextField<String> stoichiometry = (TextField<String>) getPage().get("participantForm:stoichiometry");
        stoichiometry.setEnabled(enabled);

        Button submitParticipantButton = (Button) getPage().get("participantForm:addParticipant");
        submitParticipantButton.setEnabled(enabled);

        sboTextField.setEnabled(enabled);

        // the participant list delete buttons
        ListView<Participant> participantList = (ListView<Participant>) getPage().get("participantForm:participantList");        
        participantList.setEnabled(enabled);
        
        // the list of parameters links
        getPage().get("parameterList").setEnabled(enabled);
        
	}

	
}