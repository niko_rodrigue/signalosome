package uk.ac.ebi.compneur.signalosome.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;


/**
 * <p>Annotated (base form Component, Interaction and Parameter)
 * 
 * @author Nicolas Rodriguez and Camille Laibe
 * @version 20100616
 */
@MappedSuperclass
@Audited
public abstract class Annotated implements Serializable
{
	public static final String SBO_URN = "urn:miriam:obo.sbo";
	public static final String SIGNALOSOME_CONTAINER_ONTOLOGY_URN = "urn:miriam:signalosome.container";
	
	
    @Id
    @GenericGenerator(name="id", strategy="uk.ac.ebi.compneur.signalosome.data.AnnotatedIdGenerator")
    @GeneratedValue(generator="id")
    protected String id;
    
    @Column(name="name")
    protected String name;
    
    @Column(name="user_id")
    protected String userId;
    

	@OneToMany(cascade=CascadeType.REMOVE)	
    @JoinColumn(name="external_id")
    @Cascade({org.hibernate.annotations.CascadeType.REMOVE})
    private List<Annotation> annotations = new ArrayList<Annotation>();
    
    @OneToMany(cascade=CascadeType.REMOVE)
    @JoinColumn(name="external_id")
    @Cascade({org.hibernate.annotations.CascadeType.REMOVE})
    private List<Comment> comments = new ArrayList<Comment>();
    
    @OneToMany
    @JoinTable(name="tag_rel",
               joinColumns = @JoinColumn( name="external_id"),
               inverseJoinColumns = @JoinColumn( name="tag_id"))
    private List<Tag> tags = new ArrayList<Tag>();


    
    
    /**
     * Default constructor.
     */
    public Annotated()
    {
        // nothing
    }
    
    
    /**
     * Indicates whether some other object is "equal to" this one.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Annotated other = (Annotated) obj;
        if (null == this.getId())
        {
            if (other.getId() != null)
            {
                return false;
            }
        }
        else if (!this.getId().equals(other.getId()))
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns a hash code value for the object.
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return (this.getId() + "_" + this.getName()).hashCode();
    }
    
    
    /**
     * Returns a string representation of the object.
     */
    @Override
    public String toString()
    {
        return getName() != null ? getName() : ""; // this.getId() + " - " + 
    }
    
    
    /**
     * Gets the list of annotations.
     * 
     * @return the list of annotations.
     */
    public List<Annotation> getAnnotations()
    {
        return this.annotations;
    }
    
    /**
     * Sets the list of annotations.
     * 
     * @param annotations
     */
    public void setAnnotations(List<Annotation> annotations)
    {
        this.annotations = annotations;
    }
    
    /**
     * Adds an annotation
     * 
     * @param annotation
     */
    public void addAnnotation(Annotation annotation)
    {
        if (null == this.annotations)
        {
            this.annotations = new ArrayList<Annotation>();
        }
        this.annotations.add(annotation);
    }
    
    /**
     * Removes an annotation
     * 
     * @param annotation
     */
    public void removeAnnotation(Annotation annotation)
    {
        this.annotations.remove(annotation);
    }
    
    /**
     * Gets the list of comments.
     * 
     * @return the list of comments.
     */
    public List<Comment> getComments()
    {
        return this.comments;
    }
    
    /**
     * Sets the list of comments.
     * 
     * @param comments
     */
    public void setComments(List<Comment> comments)
    {
        this.comments = comments;
    }
    
    /**
     * Adds a comment
     * 
     * @param comment
     */
    public void addComment(Comment comment)
    {
        if (null == this.comments)
        {
            this.comments = new ArrayList<Comment>();
        }
        this.comments.add(comment);
    }
    
    /**
     * Gets the list of tags.
     * 
     * @return the list of tags.
     */
    public List<Tag> getTags()
    {
        return this.tags;
    }
    
    /**
     * Sets a list of tags.
     * 
     * @param tags
     */
    public void setTags(List<Tag> tags)
    {
        this.tags = tags;
    }
    
    public void addTag(Tag tag) {
        this.tags.add(tag);
    }
    
    /**
     * Removes a tag
     * 
     * @param tag
     */
    public void removeTag(Tag tag)
    {
        this.tags.remove(tag);
    }
    
    /**
     * Gets the id.
     * 
     * @return the id.
     */
    public String getId()
    {
        return this.id;
    }
    
    /**
     * Sets the id.
     * 
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * Gets the name.
     * 
     * @return the name.
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * Sets the name.
     * 
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
	public Annotation getSBOAnnotation() {

		if (getAnnotations() != null && getAnnotations().size() > 0) {
			
			for (Annotation annotation : getAnnotations()) {
				if (annotation.getUrn().startsWith(SBO_URN)) {
					
					return annotation;
				}
			}
		}
		
		return null;
	}


    /**
     * Gets the user id.
     * 
     * @return the user id.
     */
    public String getUserId() {
		return userId;
	}


	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Annotation getContainerAnnotation() {

		if (getAnnotations() != null && getAnnotations().size() > 0) {
			
			for (Annotation annotation : getAnnotations()) {
				if (annotation.getUrn().startsWith(SIGNALOSOME_CONTAINER_ONTOLOGY_URN) 
						&& annotation.getQualifier().equals("occursIn")) 
				{					
					return annotation;
				}
			}
		}
		
		return null;
	}


}
