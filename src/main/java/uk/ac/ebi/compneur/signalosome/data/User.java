package uk.ac.ebi.compneur.signalosome.data;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.databinder.auth.data.DataUser;
import net.databinder.auth.data.hib.BasicPassword;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.hibernate.envers.Audited;
//import org.hibernate.validator.NotNull;


/**
 * <p>User
 * 
 * @version 20110111
 */
@Entity
@Table(name="user")
@Audited
public class User implements DataUser, Serializable
{
    @Transient
    private static final long serialVersionUID = -5034862538095773273L;
    
    @Id
    private String username;
    
    @Column(name="first_name")
    //@NotNull   the create account form does not allow that information yet
    private String firstName;
    
    @Column(name="last_name")
    //@NotNull   the create account form does not allow that information yet
    private String lastName;
    
    @Column(unique=true)
    //@NotNull   the create account form does not allow that information yet
    private String email;
    
    private String affiliation;
    private BasicPassword password;   // encrypted form
    
    @ElementCollection
	private Set<String> roles;
    
    
    /**
     * Constructor.
     */
    public User()
    {
    	this.roles = new HashSet<String>(1);
		this.roles.add(Roles.USER);   // default role
		this.password = new BasicPassword();
    }
    
    
    /**
     * Getter
     * @return the username
     */
    public String getUsername()
    {
        return this.username;
    }
    
    /**
     * Setter
     * @param username the username to set
     */
    public void setLogin(String username)
    {
        this.username = username;
    }
    
    /**
     * Getter
     * @return the firstName
     */
    public String getFirstName()
    {
        return this.firstName;
    }
    
    /**
     * Setter
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    
    /**
     * Getter
     * @return the lastName
     */
    public String getLastName()
    {
        return this.lastName;
    }
    
    /**
     * Setter
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    
    /**
     * Getter
     * @return the email
     */
    public String getEmail()
    {
        return this.email;
    }
    
    /**
     * Setter
     * @param email the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    /**
     * Getter
     * @return the affiliation
     */
    public String getAffiliation()
    {
        return this.affiliation;
    }
    
    /**
     * Setter
     * @param affiliation the affiliation to set
     */
    public void setAffiliation(String affiliation)
    {
        this.affiliation = affiliation;
    }
    
    /**
     * Getter
     * @return the password
     */
    public BasicPassword getPassword()
    {
        return this.password;
    }
    
    /**
     * Setter
     * @param password the password to set
     */
    public void setPassword(BasicPassword password)
    {
        this.password = password;
    }
    
    /**
     * Getter
     * @return the roles
     */
    public Set<String> getRoles()
    {
        return this.roles;
    }
    
    /**
     * Setter
     * @param roles the roles to set
     */
    public void setRole(Set<String> roles)
    {
        this.roles = roles;
    }
    
    /**
     * hasRole
     */
    public boolean hasRole(String role)
    {
    	return getRoles().contains(role);
	}

}
