package uk.ac.ebi.ontology.container;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;

import org.biojava.bio.seq.io.ParseException;
import org.biojava.ontology.Ontology;
import org.biojava.ontology.Synonym;
import org.biojava.ontology.Term;
import org.biojava.ontology.Triple;
import org.biojava.ontology.io.OboParser;

public class SignalosomeContainerOntology {

	/**
	 * 
	 */
	private static final String prefix = "SCO:";

	/**
	 * 
	 */
	private static Ontology sco;
	
	
	/**
	 * Contains the SignalosomeContainerOntology name and synonyms as key and the corresponding biojava Term as value.
	 */
	private static HashMap<String, Term> scoNamesMap = new HashMap<String, Term>();

	/**
	 * Contains the SignalosomeContainerOntology id as key and the corresponding biojava Term as value.
	 */
	private static HashMap<String, Term> scoIdsMap = new HashMap<String, Term>();
	
	static {
		OboParser parser = new OboParser();
		try {
			InputStream is = SignalosomeContainerOntology.class.getResourceAsStream("signalosome-containers.obo");
			sco = parser.parseOBO(new BufferedReader(new InputStreamReader(is)), "SignalosomeContainerOntology",
					"Systems Biology Ontology");
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		
		for (Term term : sco.getTerms()) {

			boolean isObsolete = false;
			try {
				term.getAnnotation().getProperty("is_obsolete");
				
				// If an exception as not been thrown, the property exist and the term is obsolete
				// We could do another test to be sure that the property value is equals to 'true'
				isObsolete = true;
				
			} catch (NoSuchElementException e) {
				// does nothing, the term is not obsolete
			}
			
			// There are terms that store the relationships, they are of the class Triple
			// so we exclude these to show only the actual SignalosomeContainerOntology ontology terms.
			if (!(term instanceof Triple) && !isObsolete && term.getDescription() != null) {

				scoIdsMap.put(term.getName(), term); // There is no id in the biojava Term class, so the id is store in the name field
				scoNamesMap.put(term.getDescription(), term); // as name is used to store the id, the description is storing the name !!
				
				if (term.getSynonyms().length > 0) {
					for (Object synonym : term.getSynonyms()) {
						scoNamesMap.put(((Synonym) synonym).getName(), term);
					}
				}
			}
		}
	}

	
	/**
	 * 
	 * @param sboTerm
	 * @return
	 */
	public static Term getTerm(int sboTerm) {
		return sco.getTerm(intToString(sboTerm));
	}

	/**
	 * 
	 * @param sboTerm
	 * @return
	 */
	public static Term getTerm(String sboTerm) {
		return sco.getTerm(sboTerm);
	}

	/**
	 * Returns the integer as a correctly formatted SignalosomeContainerOntology string. If the sboTerm
	 * is not in the correct range ({0,.., 9999999}), an empty string is
	 * returned.
	 * 
	 * @param sboTerm
	 * @return the given integer sboTerm as a zero-padded seven digit string.
	 */
	public static String intToString(int sboTerm) {
		if (!checkTerm(sboTerm)) {
			return "";
		}
		return prefix + sboNumberString(sboTerm);
	}
	

	/**
	 * This method creates a 7 digit SignalosomeContainerOntology number for the given Term identifier (if
	 * this is a valid identifier). The returned {@link String} will not contain the
	 * SignalosomeContainerOntology prefix.
	 * 
	 * @param sboTerm
	 * @return
	 */
	public static String sboNumberString(int sboTerm) {
		if (!checkTerm(sboTerm)) {
			return "";
		}
		StringBuilder sbo = new StringBuilder();
		sbo.append(Integer.toString(sboTerm));
		while (sbo.length() < 7) {
			sbo.insert(0, '0');
		}
		return sbo.toString();
	}

	/**
	 * Returns the string as a correctly formatted SignalosomeContainerOntology integer portion.
	 * 
	 * @param sboTerm
	 * @return the given string sboTerm as an integer. If the sboTerm is not in
	 *         the correct format (a zero-padded, seven digit string), -1 is
	 *         returned.
	 */
	public static int stringToInt(String sboTerm) {
		return checkTerm(sboTerm) ? Integer.parseInt(sboTerm.substring(4)) : -1;
	}

	/**
	 * Checks the format of the given SignalosomeContainerOntology integer portion.
	 * 
	 * @param sboTerm
	 * @return true if sboTerm is in the range {0,.., 9999999}, false otherwise.
	 */
	public static boolean checkTerm(int sboTerm) {
		return (0 <= sboTerm) && (sboTerm <= 9999999);
	}

	/**
	 * Checks the format of the given SignalosomeContainerOntology string.
	 * 
	 * @param sboTerm
	 * @return true if sboTerm is in the correct format (a zero-padded, seven
	 *         digit string), false otherwise.
	 */
	public static boolean checkTerm(String sboTerm) {
		boolean correct = sboTerm.length() == 11;
		correct &= sboTerm.startsWith(prefix);
		if (correct)
			try {
				int sbo = Integer.parseInt(sboTerm.substring(4));
				correct &= checkTerm(sbo);
			} catch (NumberFormatException nfe) {
				correct = false;
			}
		return correct;
	}
	
	/**
	 * Returns all the term of the SignalosomeContainerOntology ontology, either normal terms or relationship terms.
	 * 
	 * @return all the term of the SignalosomeContainerOntology ontology, either normal terms or relationship terms.
	 */
	public static Set<Term> getAllTerms() {
		
		return sco.getTerms();
	}
	
	/**
	 * Returns only the normal terms of the SignalosomeContainerOntology ontology, no relationship terms (Triple) are included.
	 * 
	 * @return only the normal terms of the SignalosomeContainerOntology ontology, no relationship terms (Triple) are included.
	 */
	public static Collection<Term> getTerms() {
		
		return scoIdsMap.values();
	}
	
	/**
	 * Return all the names and synonyms of all the SignalosomeContainerOntology ontology terms.
	 * 
	 * @return all the names and synonyms of all the SignalosomeContainerOntology ontology terms.
	 */
	public static Collection<String> getTermNames() {
	
		return new ArrayList<String>(scoNamesMap.keySet());
	}

	/**
	 * Gets the name of the SignalosomeContainerOntology term corresponding to the given SignalosomeContainerOntology id.
	 * 
	 * @param sboId an SignalosomeContainerOntology term id.
	 * @return the name of the SignalosomeContainerOntology term corresponding to the given SignalosomeContainerOntology id. Returns null if the 
	 * SignalosomeContainerOntology id does not exist. 
	 */
	public static String getTermName(String sboId) {
		
		Term sboTerm = scoIdsMap.get(sboId);
		
		if (sboTerm != null) {
			return sboTerm.getDescription();
		}
		
		return null;
	}

	/**
	 * Gets the id of the SignalosomeContainerOntology term corresponding to the given SignalosomeContainerOntology name.
	 * 
	 * @param sboName an SignalosomeContainerOntology term name.
	 * @return the id of the SignalosomeContainerOntology term corresponding to the given SignalosomeContainerOntology name. Returns null if the 
	 * SignalosomeContainerOntology name does not exist. 
	 */
	public static String getTermId(String sboName) {
		
		Term sboTerm = scoNamesMap.get(sboName); 
		
		if (sboTerm != null) {
			return sboTerm.getName();
		}
		
		return null;
	}

	public static void main(String[] args) {
		
		for (String name : SignalosomeContainerOntology.getTermNames()) {
			System.out.println(name);
		}
		
	}

}
